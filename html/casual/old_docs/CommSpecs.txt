* Assume maximum transfer rate of 1024 bytes / second.

* The entire board is transmitted to the client at once.

* The board is 16 x 16 (total of 256 cells)

* Assume the terrain does not change

* Assume cities can not be created or destroyed

* Information required by a single cell during transmission:
    - Visibility                1 bit
    - Occupied player           3 bits
    - Number of troops          6 bits
    - Vector configuration      4 bits
    = total of                  14 bits

    We can place this information in two bytes, like:

    . . . . . . . . . . . . . . . .

    ^ |---| |---------| |-----|
    |   |        |         |
    |   |        |         +-- Vector configuration
    |   |        +------------ Number of troops
    |   +--------------------- Occupied player
    +------------------------- Visibility

* If a cell is invisible, all bits in the 2 bytes
  are set to 0. Early in the game when large parts
  of the board are invisible, there will be huge
  chains of zeros, making run-length encoding
  very useful.

* 3 bits for a player gives us 8 occupancy configurations:
  one configuration for unoccupied, and then 7 configurations
  for different players (seven different players). If a cell
  is unoccupued, then the unocupied configuration is all
  zeros--and the troop number and vector configuration can
  also all be zeros, making run-length encoding more useful.

* 6 bits for number of troops give us a maximum of 64 troops
  per square. In reality, the server's game can have any
  number of troops per square that it likes, and just scale
  that value in 1-64 when it sends to the client.

* 4 bits for vector configuration give us the on/off state
  for each of the n/s/e/w vectors.

* We have two extra bits as insurance in case we come across
  something we've missed. Also, the 6 bits for number of 
  troops can probably be fiddled with in an emergency.

* 16 bits = 2 bytes
  2 bytes * 16 rows * 16 columns = 512 bytes
  Since we have maximum 1024 bytes per second transfer
  rate, this allows us to update the board twice every
  second.
