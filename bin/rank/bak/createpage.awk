#!/bin/awk

BEGIN {
	# print "start of html"
	FS="|"
	rank = 0;
}

{
	# print $1 "/" $2 ": " $4 " (" $3 ")"
	# print "hello"
	# print "<TR><TD>" + $1 "/" + $2 + "</TD><TD>" + $3 + "</TD><TD>" + $4 + "</TD></TR>"
	printf("<TR>\n")
	printf("<TD>%d</TD>\n", rank++);
	printf("<TD>%s/%s</TD>\n", $1, $2)
	printf("<TD>%s</TD>\n", $3)
	printf("<TD>%s</TD>\n", $4)
	printf("</TR>\n")
}

END {
	# print "end of html"
}
