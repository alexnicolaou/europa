#!/bin/awk

BEGIN {
	FS = "|"
}

{
	rating = $6
	num = split(rating, r, "/")
	if (r[2] == 75) {
		print r[1] "|" r[2] "|" $1 "|" $2
	}
}
