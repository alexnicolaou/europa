/*
 * @(#)ClosableFrame.java
 */
package win.ClosableFrame;

import java.awt.*;

/**
 * A simple frame that exits the applet viewer if you close it.
 */

public class ClosableFrame extends Frame {
	/**
	 * construct the frame
	 * @param title the title for the title bar
	 */
	public ClosableFrame(String title) {
		super(title);
	}

	/**
	 * override handleEvent() so that the system exits.
	 */
    public boolean handleEvent(Event e) {
        if (e.id == Event.WINDOW_DESTROY) {
            System.exit(0);
        }
		return super.handleEvent(e);
    }

}
