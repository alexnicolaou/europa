/*
 * @(#)GeoMorph.java
 */
package games.Battle.server.GeoMorph;

import java.awt.*;
import java.io.*;
import java.util.*;

/**
 * GeoMorph creates worlds based on pre-constructed 4x4 tiles of terrain. Since
 * each tile has been constructed by hand, it leads to "strategically 
 * interesting" terrain, instead of totally random terrain. Also, each tile
 * can only contain one city which helps keeps the cities spaced apart, and
 * the city goes on a particular square that "makes sense" for the particular
 * tile it is in.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class GeoMorph {
	/**
	 * a vector of all the tiles we have to choose from
	 */
	static Vector tiles;

	static void initTiles(String basename) {
		tiles = new Vector(32);

		int tileNum = 0;
		File infile = new File(basename + tileNum);
		while (infile.exists()) {
			try {
				FileInputStream file = new FileInputStream(infile);
				DataInputStream in = new DataInputStream(file);
				GeoTile tile = new GeoTile(in, tileNum);

				tiles.addElement(tile);
				tile = (GeoTile)tile.clone();
				tile.rotate90();
				tiles.addElement(tile);
				tile = (GeoTile)tile.clone();
				tile.rotate90();
				tiles.addElement(tile);
				tile = (GeoTile)tile.clone();
				tile.rotate90();
				tiles.addElement(tile);

				in.close();
				file.close();
			}
			catch (Exception e) {
				System.out.println("error " + e);
			}

			tileNum++;
			infile = new File(basename + tileNum);
		}
		System.gc();
	}

	/**
	 *  the size of the world (in tiles) that we're going to build
	 */
	public int WORLDSIZE = 4;

	/**
	 * an array to hold the world tiles.
	 */
	GeoTile[][] world;

	/**
	 * Construct a GeoMorph world. Load the tile files from the file base
	 * given in basename, with the tile number appended onto it. Create a 
	 * world that is numTiles by numTiles in size. The tile files are loaded,
	 * and from each tile 4 tiles are generated, one for the original 
	 * orientation and 3 for each 90 degree rotation. Mirror images are not
	 * used.
	 *
	 * @param basename the prefix of all the tile filenames
	 * @param numTiles the size of the world in tiles: 4 means 4x4 tiles
	 */
	public GeoMorph(String basename, int numTiles) {
		WORLDSIZE = numTiles;
		if (tiles == null) {
			initTiles(basename);

			Enumeration e = tiles.elements();
			while (e.hasMoreElements()) {
				GeoTile tile = (GeoTile)e.nextElement();
				tile.findAllCompatibleTiles(tiles);
			}
		}

		world = new GeoTile[WORLDSIZE][WORLDSIZE];

		Random rand = new Random();
		int startTile = Math.abs(rand.nextInt()) % tiles.size();
		world[0][0] = (GeoTile)tiles.elementAt(startTile);
		for (int i = 1; i < WORLDSIZE; i++) {
			Vector validTiles = world[i - 1][0].validTiles[1];
			int tnum = Math.abs(rand.nextInt()) % validTiles.size();
			world[i][0] = (GeoTile)validTiles.elementAt(tnum);
		}
		for (int y = 1; y < WORLDSIZE; y++) {
			for (int i = 0; i < WORLDSIZE; i++) {
				Vector restrictFromNorth = world[i][y-1].validTiles[2];

				Vector restrictFromWest = null;
				if (i > 0) {
					restrictFromWest = world[i - 1][y].validTiles[1];

					Vector validTiles = new Vector();
					Enumeration north = restrictFromNorth.elements();
					while (north.hasMoreElements()) {
						Object tile = north.nextElement();
						if (restrictFromWest.contains(tile))
							validTiles.addElement(tile);
					}

					if (validTiles.size() > 0) {
						int tnum = Math.abs(rand.nextInt());
						tnum = tnum % validTiles.size();
						world[i][y] = (GeoTile)validTiles.elementAt(tnum);
					}
					else {
						int tnum = Math.abs(rand.nextInt());
						tnum = tnum % restrictFromNorth.size();
						world[i][y] = (GeoTile)restrictFromNorth.elementAt(tnum);
					}
				}
				else {
					int tnum = Math.abs(rand.nextInt());
					tnum = tnum % restrictFromNorth.size();
					world[i][y] = (GeoTile)restrictFromNorth.elementAt(tnum);
				}
			}
		}

	}

	/**
	 * return the level of terrain at a particular board location
	 * @param bx the x location on the actual world
	 * @param by the y location on the actual world
	 */
	public int terrain(int bx, int by) {
		int tx = bx / 4;
		int ty = by / 4;
		int offx = bx % 4;
		int offy = by % 4;

		return world[tx][ty].tile[offx][offy];
	}

	/**
	 * chooses a tile at random and returns that tile's city hint in world
	 * co-ordinates.
	 */
	public Point getRandomCity() {
		Random rand = new Random();
		int tx = Math.abs(rand.nextInt()) % 4;
		int ty = Math.abs(rand.nextInt()) % 4;

		return new Point(tx * 4 + world[tx][ty].cityX, ty * 4 + world[tx][ty].cityY);
	}
}
