/*
 * @(#)PlayerFile.java
 */
package games.Battle.server.EuropaCore;

import games.Battle.shared.comm.PlayerInfo;
import games.Battle.shared.sys.Logger;
import games.Battle.shared.comm.WriterPool;
import games.Rating.*;

import java.util.*;
import java.io.*;

/**
 * PlayerFile is a thread that manages reading the player file, authorizing
 * players, modifying players, and periodically saving the player file.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class PlayerFile implements Runnable {
	/**
	 * the thread that periodically checkpoints the player file
	 */
	Thread checkPoint;
	/**
	 * the log file so that we can note when we checkpoint
	 */
	Logger logfile;

	/**
	 * the actual player file
	 */
	File playerFile;

	/**
	 * a vector of every id that is in the player file.
	 */
	Vector knownIds;
	/** 
	 * checkpointInterval is the delay between writes of the player file.
	 */
	public int checkpointInterval = 1200000;

	EuropaCore server;
	/**
	 * construct and start the player file thread.
	 * @param l the log file 
	 * @param filename the path to the player file
	 */
	public PlayerFile(EuropaCore s, Logger l, String filename) throws java.io.IOException {
	    this(l, filename);
	    server = s;
	}
	public PlayerFile(Logger l, String filename) throws java.io.IOException {
		playerFile = new File(filename);
		logfile = l;

		readPlayerFile();

		start();
	}

	/**
	 * reads the player file into the knownIds vector.
	 */
	void readPlayerFile() {
		knownIds = new Vector();
		try {
			FileInputStream infile = new FileInputStream(playerFile);
			BufferedInputStream inbuf = new BufferedInputStream(infile);
			DataInputStream in = new DataInputStream(inbuf);

			while (in.available() > 0) {
				String player = in.readLine();
				PlayerInfo id = new PlayerInfo(player);
				knownIds.addElement(id);
			}
		}
		catch (Exception e) {
			logfile.log("reading playerfile: " + e);
			System.exit(1);
		}
		logfile.log("finished reading player file");
	}

	/**
	 * saves the current set of players ot the player file
	 */
	synchronized void checkpoint() {
		try {
			Date d = new Date();
			long date = d.getTime();
			logfile.log("starting to checkpoint player file");

			int count = 0;
			Enumeration e = knownIds.elements();
			OutputStream outfile = new BufferedOutputStream(new FileOutputStream(playerFile));
			DataOutputStream out = new DataOutputStream(outfile);
			while (e.hasMoreElements()) {
				PlayerInfo id = (PlayerInfo)e.nextElement();
				long idDate = id.getLastLogon();
				long pointAdjust = (date - idDate) / 86400000;
				if (pointAdjust > 14) {
					Rating r = id.getRating();
					int newDeviation = 75 + (int)pointAdjust;
					if (newDeviation > r.getRatingDeviation())
						r.setRD(newDeviation);
				}
				String line = id.toString() + "\r\n";
				out.writeBytes(line);
				if (count % 1000 == 0) {
				    Thread.sleep(10);
				    server.shutdownMessage("Server is shutting down. " + count + " players saved so far.");
				}
				count++;
			}
			out.flush();
			out.close();
			server.shutdownMessage("Server shutdown complete");
			logfile.log("checkpointed player file");
		}
		catch (Exception e) {
			logfile.log("while checkpointing: " + e);
		}
	}

	/**
	 * Loop forever, sleeping and checkpointing periodically
	 */
    public void run() {
		int lastPacketCount = WriterPool.getPacketCount();
		int numIdles = 0;

		Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

		while (true) {
			try { 
				Thread.sleep(checkpointInterval);
				checkpoint();
			}
			catch (Exception e) {
			}

			int packetCount = WriterPool.getPacketCount();
			if (packetCount == lastPacketCount) {
				numIdles++;
				logfile.log("idle period count is now " + numIdles); 
				if (numIdles > 5 - (packetCount/2000)) {
					// shut down the server
					logfile.log("automatic shutdown.");
					System.exit(0);
				}
			}
			logfile.log("lastP is " + lastPacketCount + " -> " +packetCount); 
			lastPacketCount = packetCount;
		}
	}

	/**
	 * start the checkpointing thread
	 */
	public void start() {
		if (checkPoint != null && checkPoint.isAlive())
			return;

		checkPoint = new Thread(this, "PlayerFile checkpoint");
		checkPoint.start();
	}

	/**
	 * stop the checkpoint thread
	 */
	public void stop() {
		checkpoint();

		if (checkPoint != null && checkPoint.isAlive())
			checkPoint.stop();
	}

	/**
	 * validate a particular login attempt
	 */
	public boolean authorizePlayer(PlayerInfo id) {
		Enumeration e = knownIds.elements();
		PlayerInfo foundID = null;
		while (e.hasMoreElements()) {
			PlayerInfo test = (PlayerInfo)e.nextElement();
			if (test.sameId(id)) {
				if (test.samePass(id)) {
					return false;
				}
				return true;
			}
		}
		return true;
	}

	/**
	 * get the player info matching a given id
	 */
	public PlayerInfo getPlayerInfo(PlayerInfo id) {
		Enumeration e = knownIds.elements();
		PlayerInfo foundID = null;
		while (e.hasMoreElements()) {
			PlayerInfo test = (PlayerInfo)e.nextElement();
			if (test.sameId(id)) {
				return test;
			}
		}
		return null;
	}

	/**
	 * return true if the name is valid. caveat: this function should be
	 * pickier about what is valid
	 * @param n the name
	 */
	boolean validName(String n) { return n.length() > 0;  }

	/**
	 * create a new player based on id.
	 * @param id the id to create
	 */
	public boolean newPlayer(PlayerInfo id) {
		Enumeration e = knownIds.elements();
		PlayerInfo foundID = null;
		while (e.hasMoreElements()) {
			PlayerInfo test = (PlayerInfo)e.nextElement();
			if (test.sameId(id)) {
				// reject new player if there is any other player with
				// the same id.
				return true;
			}
		}

		if (validName(id.getHandle()) && validName(id.getName())) {
			knownIds.addElement(id);
			return false;
		}

		return true;
	}
}
