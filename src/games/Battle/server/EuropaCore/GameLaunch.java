/*
 * @(#)GameLaunch.java
 */
package games.Battle.server.EuropaCore;

import java.io.*;
import java.net.*;
import java.util.*;

import games.Rating.Rating;

import games.Battle.shared.comm.*;
import games.Battle.shared.sys.*;
import games.Battle.server.BattleGame.*;
import games.Battle.server.ServerBoard.GameReferee;

/**
 * GameTimeout watches a GameLaunch thread to make sure it actually starts.
 * If it doesn't start soon enough it assumes someone has linked out on us
 * and kills the game startup. The players will have to abort and try again.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author jay Steele
 */

class GameTimeout implements Runnable {
	/** 
	 * the game launch that we're watching
	 */
	GameLaunch launcher;
	/**
	 * the thread of the launcher
	 */
	Thread launchThread;
	/** 
	 * the log service
	 */
	Logger logfile;

	/**
	 * construct a new game timeout
	 * @param g the game we want to watch to be sure it runs
	 * @param t the thread to kill if it doesn't work
	 * @param log the logfile
	 */
	GameTimeout(GameLaunch g, Thread t, Logger log) {
		launcher = g;
		launchThread = t;
		logfile = log;
	}

	/** 
	 * delay for the timeout then check if the game is up. kill the game
	 * if it doesn't start fast enough.
	 */
	public void run() {
		boolean notimeout = true;
		logfile.log("game #"+launcher.getId()+" timeout counter started");
		while (notimeout) {
			try {
				// give the game time to startup
				Thread.sleep(30000);
				notimeout = false;
			}
			catch (Exception e) {}
		}

		// if the game isn't running after the delay someone
		// has either dropped out on us or is so badly lagged that
		// there's no point anyway
		if (launchThread.isAlive() && !launcher.isStarted()) {
			logfile.log("Killing game #"+launcher.getId()+" due to timeout");
			launchThread.stop();
			launcher.fail();
		}
		else
			logfile.log("game #"+launcher.getId()+" timeout: game is running");
	}
}

/**
 * GameLaunch performs the function of starting and refereeeing the game.
 * It deals with collecting the player sockets when they join a new game,
 * and actually launchiing the  BattleGame thread for the game. At game over
 * time, it adjusts the players ratings according to who won.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author jay Steele
 */

class GameLaunch implements Runnable, GameReferee {
	/**
	 * the log file service
	 */
	Logger logfile;
	/**
	 * the game info for the game we're launching
	 */
	GameInfo game;
	/**
	 * an array to collect the player sockets in
	 */
	Socket player[];
	/**
	 * a pointer back to the main server
	 */
	EuropaCore server;

	/**
	 * a vector to record when people died
	 */
	Vector deathOrder;

	/**
	 * set up to launch the game
	 * @param game the game that is starting
	 * @param s the server that started this game
	 * @param logfile the logfile for logging errors
	 */
	public GameLaunch(GameInfo game, EuropaCore s, Logger logfile) {
		this.game = game;
		this.logfile = logfile;
		this.server = s;
		deathOrder = new Vector(5);
	}

	/**
	 * wait on the server socket and return a new socket when a player
	 * connects. each game has its own server socket that we wait on.
	 * @param ss the server socket to wait on
	 */
	Socket getPlayer(ServerSocket ss) {
		// accept an incoming connection
		Socket s = null;
		while (true) {
			try {
				s = ss.accept();
				return s;
			} catch (InterruptedIOException e) {
				logfile.log("timed out waiting for player; looping.");
			} catch (Exception e) {
				return null;
			}
		}

	}

	/** 
	 * a flag to tell us if the game is running yet
	 */
	boolean started = false;

	/**
	 * Create a thread for ourselves and run it. Create a timeout thread
	 * to clean up this thread if we never actually get all the players.
	 */
	public void start() {
		Thread launchThread = new Thread(this, "Game Launch");
		launchThread.start();
		GameTimeout killMe = new GameTimeout(this, launchThread, logfile);
		Thread timeoutThread = new Thread(killMe, "Game Timeout");
		timeoutThread.start();
	}


	ServerSocket ss = null;
	/** 
	 * Loop waiting for all the players to join. Once eveeryone is in, 
	 * launch a new BattleGame thread to actually run the game.
	 */
	public void run() {

		try {
			int PORT = 5100 + game.getId();

			// open a new server socket on port PORT
			try {
				ss = new ServerSocket(PORT, 6);
				ss.setSoTimeout(500);
			} catch (Exception e) {
				server.chatMessage(null, "Game #" + game.getId() + " is dead. Don't use it.");
				logfile.log("couldn't create new game on port #"+PORT);
				logfile.log(e.toString());
				fail();
				return;
			}

			int numPlayers = game.numberNeeded();
			player = new Socket[numPlayers];
			Socket[] prePlay = new Socket[numPlayers];

			int[] playerMapping;
			playerMapping = new int[numPlayers];

			for (int i = 0; i < numPlayers; i++) {
				prePlay[i] = getPlayer(ss);
				if (prePlay[i] == null) {
					fail();
					logfile.log("player dropped out on game #"+game.getId());
					return;
				}
				try {
					PlayerInfo p = new PlayerInfo();
					p.readFrom(prePlay[i].getInputStream());
					int mapTo = -1;
					for (int q = 0; q < game.players.length; q++) {
						if (game.players[q].sameId(p)) {
							mapTo = q;
							break;
						}
					}
					if (mapTo == -1) {
						logfile.log("illegal player in game #"+game.getId());
						fail();
						return;
					}
					logfile.log("game #"+game.getId()+": "+i+" --> "+mapTo +
								" ("+game.players[mapTo].getHandle()+ ")");
				server.chatMessage(null, "Game #" + game.getId() + ": " +
								game.players[mapTo].getHandle()+ " is connected");
					playerMapping[i] = mapTo;
				}
				catch (Exception e) {
					logfile.log("problem with a player in game #"+game.getId());
					fail();
					return;
				}
			}

			// rearrange the player list to correspond to moon picture.
			for (int i = 0; i < player.length; i++) {
				player[playerMapping[i]] = prePlay[i];
			}

			BattleGame game = new BattleGame(player, getId(), this, ss, logfile);
			started = true;
			game.start();
			server.registerGameStart(getId(), game);
		}
		catch (Exception e) {
			// quietly lose the connection.
			fail();
			return;
		}
	}

	/**
	 * report if we've started the battle game thread
	 */
	public boolean isStarted() {
		return started;
	}

	/**
	 * report on which game id we're trying to start
	 */
	public int getId() {
		return game.getId();
	}

	/**
	 * give up and clean up - one of the players hasn't joined and will never
	 * join as far as we can tell.
	 */
	public synchronized void fail() {
		try {
			if (ss != null) {
				try {
					ss.close();
					logfile.log("cleaned up game's server socket.");
				}
				catch (IOException e) {
					logfile.log("caught exception while cleaning up: " + e);
				}
			}
			if (player != null) {
				for (int i = 0; i < player.length; i++) {
					if (player[i] != null)
						try {
							player[i].close();
						}
						catch (Exception e) {}
				}
			}
		}
		finally {
			server.resetGame(game.getId());
		}
	}

	/**
	 * record the order in which players die. each player is inserted into
	 * the death order list so that ratings can later be computed.
	 */
	public void playerDied(int playerNum) {
		logfile.log("REFEREE NOTICES: "+playerNum+" is out of cities.");
		Integer playerID = new Integer(playerNum);
		for (int i = deathOrder.size()-1; i >= 0; i--) {
			if (deathOrder.elementAt(i).equals(playerID)) {
				deathOrder.removeElementAt(i);
			}
		}
		deathOrder.insertElementAt(playerID, 0);
	}

	static String placeDesc[] = { 
		    "Winner",
		    "Second",
		    "Third",
		    "Fourth",
		    "Fifth"
	};
	/**
	 * clean up, the game has ended. Adjust all the ratings.
	 */
	public void gameOver() {
		if (deathOrder.size() == 0) {
			logfile.log("REFEREE NOTICES: bum game");
		}
		else {
			String handle[] = new String[deathOrder.size()];
			Rating originalRatings[] = new Rating[deathOrder.size()];
			Rating evaluateRatings[] = new Rating[deathOrder.size()];
			logfile.log("REFEREE NOTICES: game #"+game.getId()+" ended.");
			server.registerGameEnd(game.getId());
			server.chatMessage(null, "Game #" + game.getId() + " ended:");
			Enumeration e = deathOrder.elements();
			int i = 0;
			while (e.hasMoreElements()) {
				Integer died = (Integer)e.nextElement();
				int d = died.intValue();
				evaluateRatings[i] = game.players[d].getRating();
				originalRatings[i] = new Rating(evaluateRatings[i]);
				handle[i] = game.players[d].getHandle();
			//server.chatMessage(null, "Rating for " + game.players[d].getHandle() + " was: " + evaluateRatings[i]);
				i++;
			}

			Rating.calculateRatings(evaluateRatings);
			for (i = 0; i < evaluateRatings.length; i++)
			server.chatMessage(null, "\t" + placeDesc[i] + "\t" + handle[i] + "\t" + evaluateRatings[i] + " (was " + originalRatings[i] + ")");
		}
		server.resetGame(game.getId());
	}
}
