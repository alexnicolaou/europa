/*
 * @(#)EuropaCore.java
 */
package games.Battle.server.EuropaCore;

import java.io.*;
import java.net.*;
import java.util.*;

import games.Battle.shared.comm.*;
import games.Battle.shared.sys.*;
import games.Battle.server.BattleGame.*;

/**
 * EuropaCore is the main shell of the server. It manages mutiple connections
 * coming in from the outside, co-ordinates the player file process, and
 * spawns player threads to allow the players to do things in the game.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class EuropaCore implements Runnable {
	/**
	 * store the message of the day
	 */
	Vector motd;

	/**
	 * the thread that the server uses to run itself.
	 */
	Thread serverThread;
	/**
	 * a vector of who is currently logged onto the game
	 */
	public Vector who;

	/**
	 * the log file service
	 */
	Logger logfile;
	/**
	 * the player file thread. this object manages adding and updating
	 * players in the player file and periodically saves the player file
	 */
	PlayerFile playerFile;

	/**
	 * the most games we'll allow to ever run simulteneously.
	 */
	public static final int MAX_GAMES = 8;
	/**
	 * an array of Game Information for each game in the server
	 */
	public GameInfo[] games;
	/**
	 * an array of actual game threads for each game in the server.
	 */
	public BattleGame[] gameProc;

	/** 
	 * maximum connections the server will allow 
	 */
	int maxConnections = 128;
	/** 
	 * port number the server listens on 
	 */
	int port = 5000;

	/**
	 * gameUpdateId is incremented any time any game info changes. The player
	 * threads watch this ID and when it changes retransmit all the games to
	 * their player so that the player's displays are up to date.
	 */
	int gameUpdateId = 1;
	/**
	 * whoUpdateId is incremented any time the who list changes. The player
	 * threads watch this ID and when it changes retransmit the who list to
	 * their player so that the player's displays are up to date.
	 */
	int whoUpdateId = 1;

	/**
	 * chatUpdateId is incremented any time the chat data change. The player
	 * threads watch this ID and when it changes retransmit the chat data to
	 * their player so that the player's displays are up to date.
	 */
	int chatUpdateId = 1;
	/**
	 * the maximum number of chat messages that the server will store up.
	 * if you're so lagged that you miss this many messages you really can't
	 * play a realtime game anyway.
	 */
	static final int MAX_CHATHISTORY = 30;
	/**
	 * the ID numbers of each chat message in the history
	 */
	int[] chatIds;
	/**
	 * a history of chat messages to allow the player's thread to lag behind
	 * on actually transmitting the chats to the player, so that chatting
	 * can avoid having a any impact on game play.
	 */
	String[] chatHistory;

	/**
	 * report the last game update id
	 */
	public int lastGameUpdate() {
		return gameUpdateId;
	}

	/**
	 * report the last who update id
	 */
	public int lastWhoUpdate() {
		return whoUpdateId;
	}

	/**
	 * report the last chat update id
	 */
	public int lastChatUpdate() {
		return chatUpdateId;
	}

	/**
	 * allow a player to join a game identified by its unique id
	 * @param gameId the game to join
	 * @param id the id of the player who wants to join the game
	 */
	public void joinGame(int gameId, PlayerInfo id) {
		if (!games[gameId].isRunning()) {
			games[gameId].join(id);
			gameUpdateId++;

			if (games[gameId].isRunning()) {
				GameLaunch launcher=new GameLaunch(games[gameId],this,logfile);
				launcher.start();
			}
		}
		else {
			if (games[gameId].isInGame(id)) {
				logfile.log("CHEAT: Player " + id.getHandle() + 
							" attempting to observe own game.");
				return;
			}
			// add this player as an observer to the game
			logfile.log("Attempting to add observer to game #"+gameId+".");
			gameUpdateId++;
			int timeoutCounter = 0;
			while (gameProc[gameId] == null && timeoutCounter < 4) {
				try {
					logfile.log("sleeping for addObserver for #"+gameId+".");
					Thread.sleep(1000);
				}
				catch (Exception e) {}
				timeoutCounter++;
			}
			if (gameProc[gameId] == null)
				return;
			logfile.log("calling addObserver for #"+gameId+".");
			gameProc[gameId].addObserver(id);
		}
	}

	/**
	 * clear all the players out of a game that is over or failed to start.
	 * @param gameId the game to reset
	 */
	public void resetGame(int gameId) {
		games[gameId].reset();
		gameUpdateId++;
		whoUpdateId++;
	}

	/**
	 * allow a player to decline to be in a game before it has actually 
	 * begun. If the game is in progress the player must forfeit from the 
	 * game window rather than the moons window.
	 * @param gameId the game to quit
	 * @param id the player who wants to bail out
	 */
	public void quitGame(int gameId, PlayerInfo id) {
		if (!games[gameId].isRunning()) {
			games[gameId].quit(id);
			gameUpdateId++;
		}
	}

	/**
	 * Notice that a game has actually begun.
	 */
	public void registerGameStart(int gameId, BattleGame game) {
		gameProc[gameId] = game;
		logfile.log("Game #"+gameId+" started.");
	}

	/**
	 * Notice that a game is over
	 */
	public void registerGameEnd(int gameId) {
		gameProc[gameId] = null;
		logfile.log("Game #"+gameId+" ended.");
	}

	/**
	 * start the player file thread
	 */
	void readKnownIds() {
		try {
			playerFile = new PlayerFile(this, logfile, "players.dat");
		}
		catch (Exception e) {
			logfile.log("fatal error: couldn't read player file");
			System.exit(1);
		}
	}

	/**
	 *  construct the EuropaCore server. command line arguments are passed in
	 * to allow the user to configure some of the basic parameters like what
	 * port to run on nad how many players may connect at once.
	 * @param argv command line arguments
	 */
	public EuropaCore(String argv[]) {
		if (argv.length > 2) {
			System.out.println("usage: battle [port #] [max players]");
			System.out.println("       e.g. battle 5000 16");
			System.exit(1);
		}
		if (argv.length == 2) {
			try {
				maxConnections = Integer.valueOf(argv[1]).intValue();
			}
			catch (Exception e) {
				System.out.println("couldn't parse maxplayers="+argv[1]);
				System.out.println("ignoring argument, maxplayers=" + 
									maxConnections);
			}
		}
		if (argv.length >= 1) {
			try {
				port = Integer.valueOf(argv[0]).intValue();
			}
			catch (Exception e) {
				System.out.println("couldn't parse port="+argv[0]);
				System.out.println("ignoring argument, port=" + 
									port);
			}
		}

		logfile = new Logger();
		readKnownIds();

		gameProc = new BattleGame[MAX_GAMES];
		games = new GameInfo[MAX_GAMES];
		int numPlay = 2;
		for (int i = 0; i < games.length; i++) {
			games[i] = new GameInfo(numPlay);
			numPlay++;
			if (numPlay > Symbols.MAX_PLAYERS)
				numPlay = 2;
		}


		chatIds = new int[MAX_CHATHISTORY];
		chatHistory = new String[MAX_CHATHISTORY];
		for (int i = 0; i < chatIds.length; i++) {
			chatIds[i] = 0;
			chatHistory[i] = new String("");
		}
		chatIds[0] = 0;
		chatHistory[0] = "System: server started.";
	}

	/**
	 * register a chat message from user "id"
	 * @param id the player sending the message
	 * @param msg the message to be sent
	 */
	public void shutdownMessage(String msg) {
	    if (shutdownFlag)
		chatMessage(null, msg);
	}
	public void sysMessage(String msg) {
	    chatMessage(null, msg);
	}
	public void chatMessage(PlayerInfo id, String msg) {
		int msgNum = chatUpdateId % MAX_CHATHISTORY;
		chatIds[msgNum] = chatUpdateId;
		if (id != null)
			chatHistory[msgNum] = id.getHandle() + ": " + msg;
		else
			chatHistory[msgNum] = "System: " + msg;
		logfile.log("chat - " + chatHistory[msgNum]);
		chatUpdateId++;
	}

	/**
	 * retrieve a message from the chat history
	 * @param messageId the message to fetch
	 */
	public String getChatMessage(int messageId) {
		int msgIndex = -1;
		for (int i = 0; i < MAX_CHATHISTORY; i++) {
			if (chatIds[i] == messageId) {
				msgIndex = i;
				break;
			}
		}
		if (msgIndex != -1)
			return chatHistory[msgIndex];
		return null;
	}

	/**
	 * block waiting for a player to connect to the given server socket
	 * @param ss the socket that the server is listening on
	 */
	public Socket getPlayer(ServerSocket ss) {
		// accept an incoming connection
		Socket s = null;
		while (true) {
			try {
				s = ss.accept();
				return s;
			} catch (InterruptedIOException e) {
				logfile.log("accept timed out waiting for observer; looping");
			} catch (Exception e) {
				logfile.log( "accepting connection: " + e );
			}
		}

	}

	/**
	 * start the client
	 * @param argv command line arguments
	 */
	public static void main(String argv[]) {
		EuropaCore server = new EuropaCore(argv);
		server.start();
	}

	/**
	 * actually initiate the thread that runs the server
	 */
	public void start() {
		if (serverThread != null)
			return;

		logfile.log("Starting server on port #" + port);
		logfile.log("Max players = " + maxConnections);

		readMOTD();

		serverThread = new Thread(this, "Login Connections");
		serverThread.start();
	}

	boolean shutdownFlag = false;
	/**
	 * shutdown the server
	 */
	public void stop() {
		shutdownFlag = true;
		playerFile.checkpoint();
		String msg = "Server shutdown completed. Exit.";
		chatMessage(null, msg);
		try {
			Thread.sleep(500);
		}
		catch (Throwable t) {
		}
		logfile.log(msg);
		System.exit(0);
	}


	/**
	 * loop forever getting connections and spawning player threads
	 */
	public void run() {
		ServerSocket ss = null;

		// open a new server socket on port port
		try {
			ss = new ServerSocket(port);
		} catch (Exception e) {
			logfile.log("couldn't start server; someone must be " +
							   "using port #" + port);
			System.exit(1);
		}

		who = new Vector();
		while (true) {
			// loop forever, blocking to wait for new players to join.
			Socket player = getPlayer(ss);

			if (who.size() < maxConnections) {
				// update whoUpdateId to clean out any bad records before this
				// login starts
				whoUpdateId++;

				PlayerConnection p = new PlayerConnection(this, player);
				p.start();
			}
			else {
				// game is full, we have to reject the player.
				RejectPlayer reject = new RejectPlayer(player);
				reject.start();
			}
		}
	}

	/**
	 * login a player, assume all checks for validity have been made.
	 * @param id the player logging on
	 */
	void loginPlayer(PlayerInfo id) {
		who.addElement(id);
		whoUpdateId++;
		logfile.log(id.getHandle() + " has logged on.");
		id.touchLogin();
	}

	/**
	 * logout a player
	 * @param id the player logging out
	 */
	void logoutPlayer(PlayerInfo id) {
		who.removeElement(id);
		whoUpdateId++;

		for (int i = 0; i < games.length; i++) {
			games[i].quit(id);
		}
		gameUpdateId++;

		logfile.log(id.getHandle() + " has logged out.");
		id.touchLogin();
	}

	/**
	 * update a player's PlayerInfo. this will automatically be written to
	 * the player file on the next checkpoint
	 * @param id the player's id
	 * @param newId the player's changes to their info
	 */
	void updatePlayer(PlayerInfo id, PlayerInfo newId) {
		id.setPassword(newId.getPassword());
		id.setName(newId.getName());
		id.setMail(newId.getMail());
		whoUpdateId++;
		logfile.log(id.getHandle() + " has updated user info.");
	}

	/**
	 * shutdown the server at the request of a particular player. Only obey
	 * if the player is a wizard.
	 * @param id the player id
	 */
	synchronized void shutdown(PlayerInfo id) {
		if (id.isWizard()) {
			String msg = id.getHandle() + " has initiated server shutdown.";
			logfile.log(msg);
			chatMessage(null, msg);
			stop();
		}
		else {
			logfile.log(id.getHandle() + " attempts an illegal shutdown.");
		}
	}

	/**
	 * return the player file's official record on this player, or null if
	 * the player info passed in does not match any existing player
	 * @param id the player trying to log in
	 */
	public PlayerInfo authorizePlayer(PlayerInfo id) {
		boolean error = playerFile.authorizePlayer(id);
		if (error)
			return null;

		PlayerInfo fullID = playerFile.getPlayerInfo(id);
		loginPlayer(fullID);
		return fullID;
	}

	/**
	 * return true if the string is a valid name. caveat: this function should
	 * be picker, as a minimum scanning for PlayerInfo.FIELDSEP
	 * @param n the name
	 */
	boolean validName(String n) { return n.length() > 0;  }
	/**
	 * create a new player based on the passed in id.
	 * @param id the registration packet of hte new player
	 */
	public boolean newPlayer(PlayerInfo id) {
		return playerFile.newPlayer(id);
	}

	Vector getMOTD() {
		return motd;
	}

	void readMOTD() {
		motd = new Vector();

		try {
			File  motdFile = new File( "motd.txt" );
			FileInputStream infile = new FileInputStream( motdFile );
			DataInputStream in     = new DataInputStream( infile );

			while (in.available() > 0) {
				String   text = in.readLine();
				motd.addElement( text );
			}
		}
		catch( Exception e ) {
			logfile.log("reading motd: " + e);
		}
	}

}
