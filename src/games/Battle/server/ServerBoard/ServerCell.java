/*
 * @(#)ServerCell.java
 */
package games.Battle.server.ServerBoard;

import games.Battle.shared.sys.*;

/**
 * ServerCell implements functionality for game squares  that is specific to
 * the server's needs.
 *
 * @version 1.00
 * @author Jay Steele
 * @author Alex Nicolaou
 */

public class ServerCell extends Cell
{
	/**
	 * a boolean list of which players can see this cell. visible[player] is
	 * true if the given player can see the cell.
	 */
	boolean visible[] = {false, false, false, false, false};

	int reserves = 0;

	public void setReserves(int r) {
		reserves = r*10;
	}

	public int getMinTroops() { return reserves; }

	/**
	 * the number of troops in each direction that are in combat. each turn
	 * if troops from an incoming pipe survive they are accumulated here to
	 * fight again on the following turn.
	 */
	int fighting[] = { 0, 0, 0, 0};

	/**
	 * number of incoming paratroops from each opponent
	 */
	int paratroops[] = { 0, 0, 0, 0, 0 };

	/**
	 * number of incoming shells from each opponent
	 */
	int shells[] = { 0, 0, 0, 0, 0 };

	/**
	 * construct a new server cell at the given location
	 * @param r the row
	 * @param c the column
	 */
	public ServerCell(int r, int c) {
		super(r,c);
	}

	/**
	 * return true if the given player can see this cell, false
	 * otherwise
	 */
	public boolean isVisible(int player) 	{ 
		if (player == Symbols.UNOCCUPIED)
			return true;
		return visible[player]; 
	}

	/**
	 * set the visibility state for this cell
	 */
	public final void setVisible(int player, boolean state) {
		setModified();
		visible[player] = state;
	}

	/**
	 * tell us how many fighters are coming in from direction "d". These armies
	 * are "leftovers" from the last turn's combat.
	 * @param d the direction
	 */
	public final int getFighters(int d) {
		int f = fighting[d];
		fighting[d] = 0;

		return f;
	}
	/**
	 * sets how many fighters are left over on a particular direction's pipe.
	 * @param d the direction
	 * @param f the number of units
	 */
	public final void setFighters(int d, int f) {
		if (f > 0)
			fighting[d] = f;
		else
			fighting[d] = 0;
	}

	/**
	 * set paratroops coming into the cell from a particular player.
	 * @param player the player number
	 * @param num ignored
	 */
	public final int paratroop(int player, int num) {
		if (paratroops[player] == 0) {
			paratroops[player] = 5;
			return 5;
		}
		else if (paratroops[player] < 15) {
			paratroops[player] = 15;
			return 10;
		}
		else if (paratroops[player] < 30) {
			paratroops[player] = 30;
			return 15;
		}

		return 0;
	}

	/**
	 * set shells coming into the cell from a particular player.
	 * @param player the player number
	 * @param num ignored
	 */
	public final int artillery(int player, int num) {
		if (shells[player] == 0) {
			shells[player] = 5;
			return 5;
		}
		else if (shells[player] < 15) {
			shells[player] = 15;
			return 10;
		}
		else if (shells[player] < 30) {
			shells[player] = 30;
			return 15;
		}

		return 0;
	}

	/**
	 * empty out all incoming paratroops
	 */
	public final void clearParatroops() {
		for (int i = 0; i < paratroops.length; i++) 
			paratroops[i] = 0;
	}

	/**
	 * empty out all incoming shells
	 */
	public final void clearArtillery() {
		for (int i = 0; i < shells.length; i++) 
			shells[i] = 0;
	}

	/**
	 * return how many paratroops of player p are incoming
	 */
	public final int getParatroops(int p) {
		return paratroops[p];
	}

	/**
	 * return how many shells from player p are incoming
	 */
	public final int getShells(int p) {
		return shells[p];
	}
}
