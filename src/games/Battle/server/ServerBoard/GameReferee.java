/*
 * @(#)GameReferee.java
 */
package games.Battle.server.ServerBoard;

/**
 * The game referee interface defines how the board and battle game can 
 * report to a third observer what order the players died in and when the
 * game is over. This allows the referee to do any cleanup tasks associated
 * with the game, like announce the winner or compute ratings.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public interface GameReferee {
	/** 
	 * inform the referee that a particular player is dead.
	 */
	public void playerDied(int playerNum);
	/** 
	 * inform the referee that the game is over
	 */
	public void gameOver();
}
