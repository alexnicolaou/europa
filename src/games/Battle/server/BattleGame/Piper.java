/*
 * @(#)Piper.java
 */
package games.Battle.server.BattleGame;

import java.lang.*;
import java.io.*;

import games.Battle.shared.comm.*;
import games.Battle.shared.sys.*;
import games.Battle.server.ServerBoard.*;

/** 
 * Piper watches the client for input and executes the client's requests for
 * pipes, guns, paratroops, and whatever else the client wants to do.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steeele
 */

class Piper implements Runnable {
	/**
	 * the thread that the piper uses to run itself
	 */
	Thread piperThread;

	/**
	 * the board that the player is playing on
	 */
	ServerBoard board = null;
	/**
	 * the stream of input from the player
	 */
	InputStream is = null;
	/**
	 * the game id of the game the player is playing
	 */
	int gameId;
	/**
	 * the number of the player in this game
	 */
	int p;

	/**
	 * turnsBehind is the number of boards behind that this player is
	 */
	int turnsBehind = 0; 

	public final int getTurnsBehind() { 
		int ret = turnsBehind;
		turnsBehind = 0;
		return ret; 
	}

	/**
	 * the log service
	 */
	Logger logfile;

	/**
	 * constructs a piper object, and starts a thread in it to run it.
	 * @param board the board that the player is playing on
	 * @param is the input stream frmo the player
	 * @param g the game id of the game the player is in
	 * @param playernum the number of the player
	 * @param l the log file service
	 */
	Piper(ServerBoard board, InputStream is, int g, int playernum, Logger l) {
		this.board = board;
		this.is = is;
		this.logfile = l;
		gameId = g;
		p = playernum;
		String piperName = "Piper p=" + p + " g=" + g; 
		piperThread = new Thread(this, piperName);
		piperThread.start();
	}

	/**
	 * The run method for this thread. Waits for commands,
	 * verifies their validity, and executes them.
	 */
	public void run() {
		try {
			while (true) {
				try {
					// pause to give other threads a chance to run
					Thread.sleep(10);
				}
				catch (Exception e) { }

				Command cmd = new Command();
				try {
					cmd.readFrom(is);
				} catch (Exception e) {
					logfile.log("Game #"+gameId+" - player "+p+" vanished.");
					board.killPlayer(Symbols.PLAYER_SYMBOL[p]);
					return;
				}

				byte playerTurn = cmd.getTurnNumber();
				byte gameTurn = board.getTurnNumber();

				int diff = gameTurn - playerTurn;
				if (diff < 0)
					diff = (100 - playerTurn) + gameTurn;
				turnsBehind = diff;
				if (diff > 1) {
					logfile.log("Game #"+gameId+" - player "+p+" is " + diff + " turns behind.");
				}

				if (cmd.getId() >= Symbols.NORTH_PIPE && 
					cmd.getId() <= Symbols.WEST_PIPE) {

					Cell cell = board.getCell(cmd.getRow(), cmd.getCol());
					if (cell.getOccupancy() == Symbols.PLAYER_SYMBOL[p]) {
						if (cell.getPipe(cmd.getId())) {
							cell.setPipe(cmd.getId(), false);
						} else {
							cell.setPipe(cmd.getId(), true);
						}
					}
				}
				else if (cmd.getId() == Symbols.CLEAR_PIPES) {
					Cell cell = board.getCell(cmd.getRow(), cmd.getCol());
					if (cell.getOccupancy() == Symbols.PLAYER_SYMBOL[p]) {
						cell.clearPipes();
					}
				}
				else if (cmd.getId() == Symbols.RESERVES) {
					int resr = cmd.getRow();
					int resc = cmd.getCol();
					int percent = cmd.getX();
					ServerCell cell = (ServerCell)board.getCell(resr, resc);
					if (cell.getOccupancy() == Symbols.PLAYER_SYMBOL[p])
						{

						cell.setReserves(percent);
						}
				}
				else if (cmd.getId() == Symbols.GUN) {
					// first confirm that the source is owned by this player
					int srcr = cmd.getRow();
					int srcc = cmd.getCol();
					int dstr = cmd.getX();
					int dstc = cmd.getY();
					int offr = srcr - dstr;
					int offc = srcc - dstc;
					ServerCell src = (ServerCell)board.getCell(srcr, srcc);
					if (src.getOccupancy() == Symbols.PLAYER_SYMBOL[p]
						&& Math.abs(offr) <= Rules.gunRange
						&& Math.abs(offc) <= Rules.gunRange) 
					{
						// we have a valid gun command, so start a-gunning
						try {
							ServerCell dest = (ServerCell)board.getCell(dstr, dstc);
							if (src.getTroops() > 15 
								&& dest.getOccupancy() != Symbols.UNOCCUPIED) 
							{
								int numSent = dest.artillery(src.getOccupancy(), 1);
								src.setTroops(src.getTroops() - numSent);

								if (src.getTroops() <= 0)
									src.setTroops(1);
							}
						}
						catch (Exception e) {}
					}
				}
				else if (cmd.getId() == Symbols.PARATROOP) {
					// first confirm that the source is owned by this player
					int srcr = cmd.getRow();
					int srcc = cmd.getCol();
					int dstr = cmd.getX();
					int dstc = cmd.getY();
					int offr = srcr - dstr;
					int offc = srcc - dstc;
					ServerCell src = (ServerCell)board.getCell(srcr, srcc);
					if (src.getOccupancy() == Symbols.PLAYER_SYMBOL[p]
						&& Math.abs(offr) <= Rules.parRange
						&& Math.abs(offc) <= Rules.parRange) 
					{
						// we have a valid paratroop command so start a-paratroopin
						try {
							ServerCell dest = (ServerCell)board.getCell(dstr, dstc);
							if (src.getTroops() > 15)
							{
								int numSent = dest.paratroop(src.getOccupancy(), 1);
								src.setTroops(src.getTroops() - numSent);

								if (src.getTroops() <= 0)
									src.setTroops(1);
							}
						}
						catch (Exception e) {}
					}
				}
				else if (cmd.getId() == Symbols.SURRENDER) {
					logfile.log("Player "+Symbols.PLAYER_SYMBOL[p]+" Surrendered.");
					board.killPlayer(Symbols.PLAYER_SYMBOL[p]);

					// stop the piper thread, the player is no longer allowed
					// to continue playing
					piperThread.stop();
				}
				else {
					logfile.log("Command ID: " + cmd.getId() + " not used!");
					logfile.log("** " + cmd);
				}
			}
		}
		catch (Exception e) {
			logfile.log("PIPER DIED: Exception: "+e.toString());
		}
	}

}
