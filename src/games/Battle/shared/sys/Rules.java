/*
 * @(#)Rules.java
 */
package games.Battle.shared.sys;

/**
 * A collection of constants used to control the various parameters of the
 * game. In the future some of these parameters may be settable on a per
 * game basis.
 *
 * @version 1.00
 * @author Jay Steele
 * @author Alex Nicolaou
 */

public class Rules {
	/**
	 * the number of rows in the game world grid.
	 */
	public static int rows = 16;
	/**
	 * the number of columns in the game world grid.
	 */
	public static int cols = 16;
	/**
	 * the range of visibility 
	 */
	public static final int horizon = 2;
	/**
	 * the range of guns 
	 */
	public static final int gunRange = 2;
	/**
	 * the range of paratroops 
	 */
	public static final int parRange = 2;

	/**
	 * the minimum city size (not yet used)
	 */
	public static final int minCitySize = 0;
	/**
	 * the maximum city size (always used)
	 */
	public static final int maxCitySize = 31;

	/**
	 * the lowest terrain level (water)
	 */
	public static final int minTerrainLevel = 0;
	/**
	 * the highest terrain level (mountain peak)
	 */
	public static final int maxTerrainLevel = 7;

	/**
	 * the maximum number of players allowed in one game
	 */
	public static final int numPlayers = 5;
}
