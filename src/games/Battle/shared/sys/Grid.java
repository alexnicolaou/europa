/*
 * @(#)Grid.java
 */

package games.Battle.shared.sys;

/**
 * Grid is a container which eases access to data arranges in (r,c)
 * grids. 
 *
 * @version 1.0 04/02/96
 * @author Jay Steele
 * @author Alex Nicolaou
 */

public class Grid {

	/**
	 * a flat array of data
	 */
	Object[] data;
	/**
	 * the number of rows and columns
	 */
	int rows, cols;

	/**
	 * Construct a grid with r rows and c columns.
	 * @param r the number of rows in the grid
	 * @param c the number of columns in the grid
	 */
	public Grid(int r, int c) {
		rows = r;
		cols = c;
		data = new Object[rows*cols];
		for (int i=0; i<data.length; i++) {
			data[i] = null;
		}
	}

	/**
	 * Return the number of rows in this grid
	 */
	public final int getNumRows() 	{ return rows; }

	/**
	 * Return the number of colums in this grid
	 */
	public final int getNumCols()	{ return cols; }

	/**
	 * Return an index into a contiguous array which represents
	 * the given (r,c) pair.
	 * @param r the row
	 * @param r the column
	 */
	private final int getOffset(int r, int c) {
		return (r*cols+c);
	}

	/**
	 * Set the value of the cell in the given (row, col) location
	 * with the given object.
	 * @param row the row
	 * @param col the column
	 * @param cell the data to place in that cell
	 */
	public final void setCell(int row, int col, Object cell) {
		data[getOffset(row,col)] = cell;
	}

	/**
	 * Return the data located in the given cell location
	 * @param row the row
	 * @param col the column
	 */
	public final Object getCell(int row, int col) {
		return data[getOffset(row, col)];
	}

	/**
	 * Return the contiguous array of data representing this
	 * grid. The data is arranged in row-major order: for example,
	 * in a 2x2 grid, the first 2 elements in the array represent the
	 * first row, and the second two elements represent the second
	 * row.
	 */
	public final Object[] getData() { return data; }

	/**
	 * Set the data for this grid object. Will raise an 
	 * IllegalArgumentException if the data block is not the correct size.
	 * @param data the data block
	 */
	 public final void setData(Object[] x) {
	 	if (x.length != rows*cols)
			throw new IllegalArgumentException("data array is not the correct size");
		data = x;
	 }
}
