/*
 * @(#)Logger.java
 */
package games.Battle.shared.sys;

import java.util.Date;

/**
 * A log service class intended for producing log files. Currently simply
 * logs messages to the System.out stream preceeded by the date at which
 * the message was logged.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class Logger {
	public Logger() {
		log("log service started");
	}

	public synchronized void log(String message) {
		Date now = new Date();
		System.out.println(now + ": " + message);
	}
}
