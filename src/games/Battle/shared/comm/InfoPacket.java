/*
 * @(#)InfoPacket.java
 */
package games.Battle.shared.comm;

import java.io.*;
import java.util.*;

/**
 * InfoPacket contains a message to the user. It is used primarily for
 * transmitting information back and forth during the login process, and
 * also for transmitting chat messages.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class InfoPacket extends BattlePacket
{
	/**
	 * the message that this InfoPacket encodes.
	 */
	String infoString; 
	/**
	 * the error level reports how severe this message is
	 */
	byte errorLevel;

	/**
	 * Info is the informational error level: this message does not need to
	 * be reported to the end user.
	 */
	public final static byte Info = 0;
	/**
	 * Command is a command error level: some action should be taken as a
	 * result of this information, either automatically or under the user's
	 * directoin.
	 */
	public final static byte Command = 50;
	/**
	 * FatalError is the ultimate error level: this operation must be retried
	 * but the socket may disappear anyway if the server really gives up.
	 * Seen as the result of failed login attempts.
	 */
	public final static byte FatalError = 100;

	/**
	 * Construct a new InfoPacket to record the given information.
	 * @param info the string to be transmitted
	 * @param level the error level of the message
	 */
	public InfoPacket(String info, byte level) {
		infoString = info;
		errorLevel = level;
	}

	/**
	 * Construct an empty InfoPacket that needs to be read from an input
	 * stream.
	 */
	public InfoPacket() {
	}

	/**
	 * Converts this InfoPacket to an array of bytes for
	 * transmission.
	 */
	protected byte[] toBytes() {
		byte bytes[] = new byte[infoString.length() + 1 + 1];

		int stringLength = infoString.length();
		// max we allow is 255 characters 
		if (stringLength > 255)
			stringLength = 255;

		bytes[0] = (byte)stringLength;

		infoString.getBytes(0, stringLength, bytes, 1);

		bytes[bytes.length - 1] = errorLevel;

		return bytes;
	}

	/**
	 * Converts the given array of bytes into a particular
	 * InfoPacket.
	 * @param data the array of data to be converted
	 */
	protected void fromBytes(byte[] data) {
		int stringLength = data[0];
		if (stringLength < 0) {
			stringLength *= -1;
		}
		infoString = new String(data, 0, 1, stringLength);
		errorLevel = data[data.length - 1];
	}

	/**
	 * Produces a printout for debugging purposes.
	 */
	public void asciiDump() {
		System.out.println("info: " + infoString);
		System.out.println("level: " + errorLevel);
	}

	/**
	 * Returns a string representation, the actual text of the info message.
	 */
	public String toString() {
		return infoString;
	}

	/**
	 * Returns true if this is a fatal message.
	 */
	public boolean isFatal() {
		return errorLevel == FatalError;
	}
}
