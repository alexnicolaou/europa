/*
 * @(#)Battle.java 1.00
 */
package games.Battle.shared.comm;

import java.io.*;
import java.util.*;

/**
 * A global pool of "writer" threads for writing to a socket in a non-blocking
 * way that avoids the fact that write() calls can block indefinitely.
 *
 * @version 1.6 11/30/96
 * @author Jay Steele
 * @author Alex Nicolaou
 */

public class WriterPool implements Runnable
{
	static Vector writers = new Vector(1000);
	static Hashtable exceptionTable = new Hashtable(1000);
	static int countPackets = 0;

	boolean isFree = false;
	byte[] buffer;
	OutputStream os;


	public WriterPool(OutputStream os, byte[] buffer) {
		this.buffer = buffer;
		this.os = os;
	}

	public void run() {
		try {
			os.write(buffer, 0, buffer.length);
		}
		catch (IOException e) {
			// record the exception
			exceptionTable.put(os, e);
		}
		os = null;
		buffer = null;
		isFree = true;
	}

	static public int getPacketCount() {
		return countPackets;
	}

	static public void write(OutputStream os, byte[] buffer) 
	  throws java.io.IOException {

		countPackets++;

		os.write(buffer, 0, buffer.length);
	}
}
