/*
 * @(#)PlayerInfo.java
 */
package games.Battle.shared.comm;

import games.Rating.Rating;

import java.io.*;
import java.util.*;

/**
 * PlayerInfo records all of the information about a particular player for
 * the client and the server. Since the PlayerInfo structure contains a 
 * password, the server must be careful to only send you copies of your
 * own PlayerInfo structure: the derived WhoInfo is used to transmit info
 * about other players.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class PlayerInfo extends BattlePacket
{
	/**
	 * A separator used for storing this packet as a string, and for parsing
	 * the string representation.
	 */
	final static String FIELDSEP = "|";

	/**
	 * The user's login ID, which may be any unique login id they choose.
	 */
	String handle; 
	/**
	 * The user's real name, which hopefully is actually their real name.
	 */
	String name;
	/**
	 * The user's email address (optional).
	 */
	String mail;
	/**
	 * The user's password. This should be transmitted and stored in encrypted
	 * forms, but encryption was forbidden for use for the Java Cup 
	 * International, no doubt a side effect of the US Governments' definition
	 * of encryption algorithms as munitions of war. Next time Canada invades
	 * the US you can bet we won't be able to understand each other which will
	 * hopefully prolong the war, as any good encryption algorithm should be 
	 * able to do, proving that encryption must be secret. Which way to the
	 * border again?
	 */
	String pass;
	/**
	 * A wizard bit for the player. Wizards are allowed to shutdown the server
	 * cleanly, but have no other interesting powers yet.
	 */
	boolean wizard;
	/**
	 * The player's rating.
	 */
	Rating rating;
	/**
	 * The player's preference for whether the client should use sound or not.
	 */
	boolean sound;
	/**
	 * The player's preference for whether the client should use 3D style
	 * graphics or not.
	 */
	boolean arcade;

	long lastLoggedOn;

	/**
	 * Constructs a player info from the information usually given on the
	 * registration screen.
	 */
	public PlayerInfo(String handle, String name, String mail, String pass) {
		this.handle = handle;
		this.name = name;
		this.mail = mail;
		this.pass = pass;
		rating = new Rating();
		sound = true;
		arcade = true;
		Date d = new Date();
		lastLoggedOn = d.getTime();
	}

	/**
	 * Constructs a player info object from complete information on the
	 * player.
	 */
	public PlayerInfo(String handle, String name,String mail, String pass, boolean wizard, Rating rating, boolean sound, boolean arcade) {
		this.handle = handle;
		this.name = name;
		this.mail = mail;
		this.pass = pass;
		this.wizard = wizard;
		this.rating = rating;
		this.sound = sound;
		this.arcade = arcade;
	}

	/**
	 * Constructs a PlayerInfo object from a string representation that was
	 * obtained by calling toString() on a previous PlayerInfo object. 
	 * Currently convenient for writing and reading the player file.
	 * @param stringRep the string representation of this user
	 */
	public PlayerInfo(String stringRep) {
		StringTokenizer data = new StringTokenizer(stringRep, FIELDSEP, true); 

		handle = data.nextToken();
		data.nextToken();

		name = data.nextToken();
		data.nextToken();

		mail = data.nextToken();
		if (mail.equals("|"))
			mail = "";
		else
			data.nextToken();
		pass = data.nextToken();
		data.nextToken();

		String wiz = data.nextToken();
		if (wiz.charAt(0) == 't')
			wizard = true;
		else
			wizard = false;
		data.nextToken();

		String rstring = data.nextToken();
		rating = new Rating(rstring);
		data.nextToken();

		String snd = data.nextToken();
		if (snd.charAt(0) == 't')
			sound = true;
		else
			sound = false;
		data.nextToken();

		String arc = data.nextToken();
		if (arc.charAt(0) == 't')
			arcade = true;
		else
			arcade = false;

		if (data.hasMoreTokens()) {
			// read in last used time for player
			data.nextToken();
			String date = data.nextToken();
			lastLoggedOn = Long.parseLong(date);
		}
		else {
			// set last used time to 0
			lastLoggedOn = 0;
		}
	}

	public void touchLogin() {
		Date d = new Date();
		lastLoggedOn = d.getTime();
	}

	public long getLastLogon() { return lastLoggedOn; }

	/**
	 * Construct an empty player info object, ready to be read from an input
	 * stream.
	 */
	public PlayerInfo() {
	}

	/**
	 * Converts this PlayerInfo to an array of bytes for
	 * transmission.
	 */
	protected byte[] makeBytes(String s) {
		// java.lang.String doesn't give us a way to know how many bytes
		// we're going to get back; kludge: use knowledge of internal
		// implementation to make the correct size array
		// as an aside, it really ought to give us two bytes per char? 
		byte bytes[] = new byte[s.length() + 1];

		int stringLength = s.length();
		// max we allow is 254 characters 
		if (stringLength > 255)
			stringLength = 254;

		bytes[0] = (byte)stringLength;

		s.getBytes(0, stringLength, bytes, 1);

		return bytes;
	}

	/**
	 * catBytes takes a vector of byte arrays and concatenates them into
	 * one long byte aray which is then returned to the caller.
	 * @param strings the vector of byte arrays to be glued together
	 */
	protected byte[] catBytes(Vector strings) {
		Enumeration e = strings.elements();
		int totalLen = 0;

		while (e.hasMoreElements()) {
			byte b[] = (byte[])e.nextElement();
			totalLen += b.length;
		}

		byte b[] = new byte[totalLen];
		int pos = 0;

		e = strings.elements();
		while (e.hasMoreElements()) {
			byte subarray[] = (byte[])e.nextElement();
			for (int i = 0; i < subarray.length; i++) {
				b[pos] = subarray[i];
				pos++;
			}
		}

		return b;
	}

	/**
	 * return a byte array that represents this packet.
	 */
	protected byte[] toBytes() {
		Vector strings = new Vector(4);
		strings.addElement(makeBytes(handle));
		strings.addElement(makeBytes(name));
		strings.addElement(makeBytes(mail));
		strings.addElement(makeBytes(pass));
		byte[] data = new byte[6];
		data[0] = (byte)255;
		
		int r = (int)rating.getRating();
		int rd = (int)rating.getRatingDeviation();
		data[1] = (byte)((r & 0xff00)>>8);
		data[2] = (byte)((r & 0x00ff));
		data[3] = (byte)((rd & 0xff00)>>8);
		data[4] = (byte)((rd & 0x00ff));

		data[5] = 0;

		if (wizard)
			data[5] |= 0x01;
		if (sound)
			data[5] |= 0x02;
		if (arcade)
			data[5] |= 0x04;

		strings.addElement(data);

		return catBytes(strings);
	}

	/**
	 * Takes in a concatenated list of byte arrays and produces a vector
	 * of strings from it. A sentinel is used to distinguish the byte 
	 * arrays from the rest of the PlayerInfo packet.
	 * @param data the byte array that contains data for this packet
	 */
	protected Vector getStrings(byte[] data) {
		int pos = 0;
		Vector strings = new Vector();
		while (pos < data.length) {
			int stringLength = data[pos];

			// check for 255 because on IRIX this test fails.
			// perhaps netscape compiles with signed chars on IRIX
			// but unsigned chars on other platforms?
			if (stringLength == -1 || stringLength == 255) {
				return strings;
			}

			pos++;

			String s = new String(data, 0, pos, stringLength);
			strings.addElement(s);
			pos += stringLength;
		}

		return strings;
	}

	/**
	 * Converts the given array of bytes into a particular
	 * PlayerInfo.
	 * @param data the array of data to be converted
	 */
	protected void fromBytes(byte[] data) {
		Vector strings = getStrings(data);
		Enumeration e = strings.elements();
		handle = (String)e.nextElement();
		name = (String)e.nextElement();
		mail = (String)e.nextElement();
		pass = (String)e.nextElement();

		wizard = sound = arcade = false;

		if ((data[data.length-1]&0x01) != 0)
			wizard = true;
		if ((data[data.length-1]&0x02) != 0)
			sound = true;
		if ((data[data.length-1]&0x04) != 0)
			arcade = true;

		int r = makeInt(data[data.length-4], data[data.length-5]);
		int rd = makeInt(data[data.length-2], data[data.length-3]);
		rating = new Rating(r, rd);
	}

	/**
	 * takes two bytes and produces an integer from them. Sure, two bytes
	 * are only long enough for a short, but we want an integer anyway.
	 */
	int makeInt(byte b0, byte b1) {
		int i0 = (int)b0 & 0x00ff;
		int i1 = (int)b1 & 0x00ff;
		return (i1<<8)|i0;
	}

	/**
	 * return true if the ID's are identical ignoring case.
	 */
	public boolean sameId(PlayerInfo other) {
		return handle.equalsIgnoreCase(other.handle);
	}

	/**
	 * return true if the passwords are identical.
	 */
	public boolean samePass(PlayerInfo other) {
		return pass.equals(other.pass);
	}

	/**
	 * produce text output for this PlayerInfo
	 */
	public void asciiDump() {
		System.out.println("handle: " + handle);
		System.out.println("name: " + name);
		System.out.println("mail: " + mail);
		System.out.println("pass: " + /* pass */ "<SECRET>");
	}

	/**
	 * return the handle of this player.
	 */
	public String getHandle() { return handle; }
	/**
	 * return the password of this player.
	 */
	public String getPassword() { return pass; }
	/**
	 * return the real name of this player.
	 */
	public String getName() { return name; }
	/**
	 * return the email address of this player.
	 */
	public String getMail() { return mail; }

	/**
	 * set the password of this player.
	 */
	public void setPassword(String newpass) { pass = newpass; }
	/**
	 * set the real name of this player.
	 */
	public void setName(String newname) { name = newname; }
	/**
	 * set the email address of this player.
	 */
	public void setMail(String newmail) { mail = newmail; }

	/**
	 * convert to a string representation where all the data is in fields
	 * separated by the FIELDSEP character.
	 */
	public String toString() {
		return handle + FIELDSEP + name + FIELDSEP + mail + FIELDSEP + pass 
					  + FIELDSEP + wizard + FIELDSEP + rating + FIELDSEP 
					  + sound + FIELDSEP + arcade + FIELDSEP + lastLoggedOn;
	}

	/**
	 * return the player's rating.
	 */
	public Rating getRating() {
		return rating;
	}

	/**
	 * returns true if the player is a wizard (administrator)
	 */
	public boolean isWizard() {
		return wizard;
	}

	/**
	 * returns true if the player wants the client to use sound.
	 */
	public boolean useSound() {
		return sound;
	}

	/**
	 * returns true if the player wants the client to be arcade-style (3D).
	 */
	public boolean useArcade() {
		return arcade;
	}

	/**
	 * sets the user's sound preference (on or off)
	 */
	public void setSound(boolean b) {
		sound = b;
	}

	/**
	 * sets the user's arcade (3D look) preference (on or off)
	 */
	public void setArcade(boolean b) {
		arcade = b;
	}
}
