/*
 * @(#)TerrainInit.java
 */
package games.Battle.shared.comm;

import java.awt.Color;
import games.Battle.shared.sys.*;

/**
 * The TerrainInit packet is sent to each client prior to the 
 * start of each game, and represents the terrain of the entire
 * game board. This is somewhat unfortunate, since this means
 * a rewritten client could take advantage of this information
 * and always make the terrain visible. However, I have yet
 * to be convinced that sending terrain information in every
 * game turn is a good idea.
 *
 * @version 1.00 02/02/96
 * @author Jay Steele
 * @author Alex Nicolaou
 */
public class TerrainInit extends BattlePacket {
	/**
	 * The board
	 */
	Board board;

	/**
	 * Builds a terrain initializer for a particular board.
	 * @param board the game board from which the init data will be read/written
	 */
	public TerrainInit(Board board) {
		this.board = board;
	}

	/**
	 * Produce an array of bytes representing a terrain init
	 */
	public byte[] toBytes() {

		Board sb = board;

		byte[] result = new byte[Rules.rows*Rules.cols];
		for (int r=0; r<Rules.rows; r++) {
			for (int c=0; c<Rules.cols; c++) {
				int i = Common.RCtoIndex(r, c);
				Cell cell = sb.getCell(r, c);
				int occupancy = cell.getOccupancy();
				int terrain = cell.getTerrain();
				result[i] = Common.stuffOccupancyAndTerrain(occupancy, terrain);

				// this is now always set to blind hacked clients!
				//if (cell.getCity() > 0)
					result[i] = (byte)(result[i] | 1);
			}
		}
		return result;
	}

	/**
	 * Fill a board with the terrain initialization data in the buffer.
	 * @param buffer the terrain initialization data
	 */
	public void fromBytes(byte[] buffer) {

		Board cb = board;

		for (int r=0; r<Rules.rows; r++) {
			for (int c=0; c<Rules.cols; c++) {
				int i = Common.RCtoIndex(r, c);
				byte b = buffer[i];
				int occupancy = Common.extractOccupancy(b);
				int terrain = Common.extractTerrain(b);
				int pipes = Common.extractPipes(b);
				Cell cell = board.getCell(r, c);
				if ((b & 0x01) != 0) {
					// in terrain init first bit is base bit
					//cell.setCity();
					// this is now a blind for hacked clients!
				}
				cell.setOccupancy(occupancy);
				cell.setTerrain(terrain);
			}
		}
	}
}
