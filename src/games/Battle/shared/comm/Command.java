/*
 * @(#)Command.java
 */

package games.Battle.shared.comm;

/**
 * A command is issued from the game client to the game server,
 * and represents a request from a player to the game engine
 * to modify his/her state on the game board. There are a variety
 * of possible commands, which are detailed in the Battle.sys.Symbols
 * class. When the game server receives a command from
 * the client, it can choose to execute it, or to ignore it, if
 * the command was not legal.
 * 
 * @version 1.00 02/02/96
 * @author Jay Steele
 * @author Alex Nicolaou
 */
public class Command extends BattlePacket {
	byte player;
	short id;
	byte cell;
	short mouse;
	byte turnNumber;

	/**
	 * convert to a string representation
	 */
	public String toString() {
		return getClass().getName() + "[player=" + player + ",id=" + id
				+ ",r="+getRow()+",c="+getCol()+",x="+getX()+",y="+getY()
				+ "]";
	}

	/**
	 * Creates a default command which can then be filled using the
	 * readFrom(is) method. The server will probably be the only
	 * user of this method.
	 */
	public Command() {
		player = (byte)0;
		id = (short)0;
		cell = (byte)0;
		mouse = (short)0;
		turnNumber = 0;
	}

	/**
	 * Creates a command based on the given data, which it can then
	 * send to a data stream using the writeTo(os) method. The client
	 * is probably the only user of this method.
	 * @param player who issued this command
	 * @param id what command does this represent
	 * @param row what row on the game board did this command come from
	 * @param col what column on the game board did this command come from
	 * @param x what x-position within the given cell did the command come from
	 * @param y what y-position within the given cell did the command come from
	 */
	public Command(int player, short id, int row, int col, int x, int y, byte turnNumber) {
		this.player = (byte)player;
		this.id = id;
		this.cell = (byte)(row << 4 | (col & 0x000f));
		this.mouse = (short)(x << 8 | (y & 0x00ff));
		this.turnNumber = turnNumber;
	}


	public final byte getTurnNumber() { return turnNumber; }

	/**
	 * Dump a text version of this command, for debugging.
	 */
	public void asciiDump() {
		System.out.print("Command <");
		System.out.print((int)player+", ");
		System.out.print(id+", ");
		System.out.print("("+getRow()+", "+getCol()+"), ");
		System.out.print("("+getX()+", "+getY()+")>");
		System.out.println();
	}

	/**
	 * Return the player id for this command.
	 */
	public final int getPlayer() { return (int)player & 0x000f; }

	/**
	 * Return the command id for this command.
	 */
	public final short getId() { return id; }

	/**
	 * Return the row this command represents. This data may or may
	 * not be relevant for every command.
	 */
	public final int getRow() { return (cell >> 4) & (byte)0x0f; }

	/**
	 * Return the column this command represents. This data may or
	 * may not be relevant for every command.
	 */
	public final int getCol() { return cell & (byte)0x0f; }

	/**
	 * Return the x position within a cell this command originated from.
	 * This data may or may not be relevant for every command.
	 */
	public final int getX() { return (mouse >> 8) & (short)0x00ff; }

	/**
	 * Return the y position within a cell this command originated from.
	 * This data may or may not be relevant for every command.
	 */
	public final int getY() { return mouse & (short)0x00ff; }

	/**
	 * Convert this Command to an array of bytes for transmission.
	 */
	protected byte[] toBytes() {
		byte[] buffer = new byte[7];
		buffer[0] = player;
		buffer[1] = (byte)((id >> 4) & 0x000f);
		buffer[2] = (byte)(id & 0x000f);
		buffer[3] = cell;
		buffer[4] = (byte)((mouse >> 8) & 0x00ff);
		buffer[5] = (byte)(mouse & 0x00ff);
		buffer[6] = turnNumber;
		return buffer;
	}

	/**
	 * With the given array of bytes, fill this command object
	 * with data.
	 * @param data the data to fill the command with.
	 */
	protected void fromBytes(byte[] data) {
		if (data.length != 5) {
			// raise exception ?
		}
		player = data[0];
		id = (short)(((int)data[1] << 4) | ((int)data[2] & 0x000f));
		cell = data[3];
		mouse = (short)(((int)data[4] << 8) | ((int)data[5] & 0x00ff));
		turnNumber = data[6];
	}
}
