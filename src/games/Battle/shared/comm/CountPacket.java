/*
 * @(#)CountPacket.java
 */

package games.Battle.shared.comm;

/**
 * A special packet that indicates hoa many packets are about to follow. This
 * is used by the server before a series of WhoInfo, GameInfo, or chat packets.
 * <P>
 * The count is internally limited to 255.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class CountPacket extends BattlePacket {
	byte count;

	/**
	 * convert to a string representation
	 */
	public String toString() {
		return getClass().getName() + " [count=" + count + "]";
	}

	/**
	 * construct a CountPacket with a value of 0. Used before reading in
	 * an actual CountPacket from a stream.
	 */
	public CountPacket() {
		count = (byte)0;
	}

	/**
	 * construct a CountPacket with a particular value. Used before writing 
	 * the CountPacket to a stream.
	 * @param count the count of items that will follow this packet
	 */
	public CountPacket(int count) {
		this.count = (byte)count;
	}

	/**
	 * Dump a text version of this packet, for debugging.
	 */
	public void asciiDump() {
		System.out.println("CountPacket <" + count + ">");
	}

	/**
	 * Return the count 
	 */
	public final int getCount() { return (int)count & 0x00ff; }

	/**
	 * Convert this packet to an array of bytes for transmission.
	 */
	protected byte[] toBytes() {
		byte[] buffer = new byte[1];
		buffer[0] = count;
		return buffer;
	}

	/**
	 * With the given array of bytes, fill this object
	 * with data.
	 * @param data the data to fill the CountPacket with.
	 */
	protected void fromBytes(byte[] data) {
		count = data[0];
	}
}
