/*
 * @(#)ClientArcadeAnimatr.java
 */

package games.Battle.client.ClientApplet;

import java.awt.Graphics;
import java.awt.Image;
import java.lang.Thread;

/*
 * This simple client class is used to draw the smoke and fire
 * animations encountered during conflicts between oposing players
 * on the Europa game board. Basically the ClientArcadeAnimator
 * is started as a thread, runs for a specified number of frames
 * at a given frame rate, and then dies.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientArcadeAnimator implements Runnable {

	/**
	 * The graphics device to animate on.
	 */
	Graphics g = null;

	/**
	 * The x locationo of the animation on the board.
	 */
	int x = 0; 

	/**
	 * The y locationo of the animation on the board.
	 */
	int y = 0;

	/**
	 * The array of images to animate.
	 */
	Image imageArray[] = null;

	/**
	 * The number of frames to animate per second.
	 */
	int fps = 15;

	/**
	 * The total number of frames to animate before dying.
	 */
	int duration = 15;

	/**
	 * Construct a ClientArcadeAnimator. This runnable, when started
	 * as a thread will draw the array of images to the graphics context
	 * at the location (x,y) for dur frames at fps frames per second.
	 * @param g the Graphics context
	 * @param x the x location to draw
	 * @param y the y location to draw
	 * @param images the array of images to animate
	 * @param fps the number of frames to draw per second
	 * @param dur the total number of frames to animate
	 * @see Graphics
	 * @see Image
	 */
	public ClientArcadeAnimator(Graphics g, int x, int y, Image images[], 
								int fps, int dur) 
	{
		this.g = g;
		this.x = x;
		this.y = y;
		this.imageArray = images;
		this.fps = fps;
		this.duration = dur;
	}

	/**
	 * The main body for the ClientArcadeAnimator runnable. The method
	 * just loops and delays at appropriate values to achieve the
	 * results specified in the contructor.
	 */
	public void run() {
		long delay = (long)((float)1000.0 / (float)fps);
		for (int f=0; f<duration; f++) {
			int frame = f % imageArray.length;
			try { 
				Thread.currentThread().sleep(delay);
			} catch ( Exception e ) {}
			g.drawImage(imageArray[frame], x, y, ClientImages.applet);
		}
	}
}
