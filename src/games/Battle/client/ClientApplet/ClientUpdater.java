/*
 * @(#)ClientUpdater.java
 */
package games.Battle.client.ClientApplet;

import java.lang.Thread;

/**
 * The client updater is simple a thread that sleeps until somebody
 * wakes it up to redraw the board, and then it goes back to sleep 
 * again.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientUpdater implements Runnable {

	/**
	 * The board to obtain update information from.
	 */
	ClientBoard board;

	/**
	 * The current "look" of the client game board.
	 */
	ClientLook look;

	/**
	 * The thread whose sole purpose is to graphically update the client.
	 */
	Thread updaterThread;


	/**
	 *
	 */
	boolean running = true;

	/**
	 * Construct a ClientUpdater with the given ClientBoard 
	 * and ClientLook. 
	 * @param board the ClientBoard this updater will update from
	 * @param look the ClientLook this updater will update with
	 */
	public ClientUpdater(ClientBoard board, ClientLook look) {
		this.board = board;
		this.look = look;

		// start the updater thread
		updaterThread = new Thread(this, "ClientUpdater");
		updaterThread.start();
	}

	/**
	 * Set the look for this updater.
	 * @param look the look instance to reference from now on
	 */
	public synchronized void setLook(ClientLook look) {
		this.look = look;
	}

	/**
	 * Called to wake up the thread and update the board.
	 */
	public void update() {
	    doUpdate();
	}

	/**
	 * Physically update the contents of the display with the board
	 * information.
	 */
	protected synchronized void updateLook() {
		look.update(board);
		look.updateDisplay();
	}

	Object updateLock = new Object();

	public void waitForUpdate() {
	    synchronized(updateLock) {
		try {
		    updateLock.wait();
		}
		catch (Exception e) {}
	    }
	}

	public void doUpdate() {
	    synchronized(updateLock) {
		updateLock.notify();
	    }
	}

	/**
	 * The run method executed by the thread. The method basically
	 * loops, performing a) board updates and then b) waiting
	 * itself. The updates is "woken" up by a call to "update";
	 */
	public void run() {
		long oldtime = 0;
		long newtime = 0;
		long diff = 0;
		while (running) {

			// I will wait until somebody wakes me 
			// up to update the board
			waitForUpdate();

			if (!running) break;

			oldtime = System.currentTimeMillis();
			// I'm awake, now go and update the board
			updateLook();
			newtime = System.currentTimeMillis();
			diff = newtime - oldtime;
			if (diff > 500)
				System.out.println("Warning: it took "+diff+" milliseconds to update the client");
		}
	}

	/**
	 * Stop the updater thread from executing.
	 */
	public void stop() {
		running = false;
		doUpdate();
	}
}
