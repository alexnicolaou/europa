/*
 * @(#)ClientTimeoutAgent.java
 */
package games.Battle.client.ClientApplet;

import java.lang.*;
import java.applet.*;

/**
 * The client timeout agent is a thread who loops forever until it
 * is killed. During is life span, it marks the time, falls asleep,
 * wakes up and checks how long it has been asleep. If the client
 * wakes up and notices that it has been asleep for longer than 
 * its timeout resolution, than it will execute a ClientAbortDialog
 * asking the user if he/she wishes to abort or continue processing
 * the thread which had timed out.
 *
 * The timeout agent is used to monitor the status of socket connections.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientTimeoutAgent extends Thread {

	/**
	 * The applet the agent is timout out on.
	 */
	Applet applet;

	/**
	 * The timout resolution.
	 */
	long resolution = 0;

	/**
	 * The last time the agent was started or reset.
	 */
	long start_time = 0;

	/**
	 * A flag used to keep the thread running.
	 */
	boolean running = true;

	/**
	 * Construct a timeout agent with the given resolution and 
	 * activation message. If the timeout is tripped, then a
	 * dialog box is displayed asking if the connection should
	 * abandoned or retried.
	 * @param resolution the resolution of the agent in milliseconds
	 * @param the message to display when the agent times out
	 * @param the thread to block if the timeout is tripped
	 */
	public ClientTimeoutAgent(long resolution, Applet a, String desc) {
		super("ClientTimeoutAgent: " + desc);
		this.resolution = resolution;
		this.applet = a;
	}

	/**
	 * Return the applet instance this agent is monitoring
	 */
	public Applet getApplet() {
		return applet;
	}

	/**
	 * Halts execution of the agent
	 */
	public void halt() {
		running = false;
		reset();
		interrupt();
	}

	/**
	 * Execution method of the timeout agent.
	 */
	public void run() {
		while (running && isAlive()) {
			start_time = System.currentTimeMillis();
			try {
				Thread.currentThread().sleep(resolution);
			} catch ( InterruptedException e ) {
			}
			if (!running) 
			    break;
			long end_time = System.currentTimeMillis();
			if (end_time - start_time >= resolution) {
			    synchronized(applet) {
				ClientAbortDialog dlg = new ClientAbortDialog(applet, this, getName());
				try {
				    applet.wait();
				}
				catch (Exception e) {}
				reset();
			    }
			}
		}
	}

	/**
	 * Resets the timer for the timeout agent.
	 */
	public void reset() {
		start_time = System.currentTimeMillis();
	}

}
