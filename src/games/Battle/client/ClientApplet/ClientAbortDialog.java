/*
 * @(#)ClientAbortDialog.java
 */

package games.Battle.client.ClientApplet;

import java.awt.*;
import java.applet.*;
import java.lang.*;

/**
 * A specialized dialog class used for shutting down the game
 * client in the event of a communication problem with the
 * game server.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientAbortDialog extends Frame {

	/**
	 * The parent applet
	 */
	Applet applet;

	/**
	 * The thread which is waiting to make
	 * this dialog modal.
	 */
	Thread thread;

	/**
	 * The dialog box message.
	 */
	TextArea message;

	/**
	 * The Retry and Abort buttons panel.
	 */
	Panel buttons;

	/**
	 * The abort button.
	 */
	Button abort;

	/**
	 *  The retry button.
	 */
	Button retry;

	/*
	 * Flag indicating which button has focus.
	 */
	int focus = 0;

	/*
	 * An array of all of the buttons.
	 */
	Component[] fields;

	/**
	 * Builds the dialog box, telling it which applet to kill
	 * if the user requests it, and which thread is waiting
	 * to make the dialog box appear modal. The waiting
	 * thread is notified when the dialog box is closed.
	 * @param applet the applet to notify or kill
	 * @param thread the thread to block on to give modal behaviour
	 */
	public ClientAbortDialog(Applet applet, Thread t, String name) {
		// super(null, "Time-out warning", true);
		super("Time-out warning + [" + name + "]");

		this.applet = applet;
		this.thread = t;

		setLayout(new BorderLayout(80, 10));

		if (message != null)
			message.removeNotify();

		message = new TextArea();
		message.setEditable(false);
		String msg;
		msg = "Warning!\n";
		msg = msg+"The client application does not seem to be communicating\n";
		msg = msg+"with the server. You can attempt to regain contact by\n";
		msg = msg+"pressing \"retry\" or you can kill this particular game\n";
		msg = msg+"client and try to connect again by pressing \"abort\".\n";
		message.appendText(msg);

		add("Center", message);

		if (buttons != null)
			buttons.removeNotify();

		buttons = new Panel();
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));
		buttons.add(retry = new Button("Retry"));
		buttons.add(abort = new Button("Abort"));
		add("South", buttons);
		pack();

		fields = new Component[2];
		fields[0] = retry;
		fields[1] = abort;
		focus = 0;
		retry.requestFocus();

		show();
		toFront();
	}

	/**
	 * Processes the dialog box events
	 * @param e the event being handled
	 */
	public boolean handleEvent(Event e) {
		if (e.id == Event.WINDOW_DESTROY || e.arg == "Retry") {
			applet.notify();
			dispose();
		} 
		else if (e.id == Event.KEY_RELEASE) {
			if ((char)e.key == '\t' || (char)e.key == '\n') {
				focus++;
				focus %= fields.length;
				fields[focus].requestFocus();
			}
		}
		else if (e.arg == "Abort") {
			Frame f = (Frame)(applet.getParent());
			f.dispose();
			dispose();
			applet.notify();
			applet.destroy();
		}
		return false;
	}
}
