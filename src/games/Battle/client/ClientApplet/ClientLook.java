/*
 * @(#)ClientLook.java
 */
package games.Battle.client.ClientApplet;

import java.awt.Image;
import java.awt.Graphics;
import java.awt.Component;

/**
 * ClientLook is a base class which defines the interface (and some
 * implementation) for a set of classes used to implement the "look"
 * of the client. By deriving a new class from ClientLook and 
 * overriding the update(b) and updateTerrain() methods, an entirely
 * new look can be written for the client.
 *
 * Typically the terrain is generated once at the beginning of the
 * game after the client has received the TerrainInit packet from
 * the server. After that, updateTerrain() should not need to be
 * called again. update() is called on every turn and allows all
 * of the troops and pipes to be drawn.
 *
 * The original Europa client has two look classes: ClientLookArcade
 * and ClientLookTraditional.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public abstract class ClientLook {

	/**
	 * A reference to the ClientBoardComponent's offscreen image.
	 */
	protected Image offScreenImage = null;

	/** 
	 * A graphics context to the offscren image.
	 */
	protected Graphics offGC = null;

	/**
	 * A graphics context to the ClientBoardComponent's drawing area.
	 */
	protected Graphics onGC = null;

	/**
	 * A reference to the ClientBoardComponent instance for this game board.
	 */
	protected ClientBoardComponent component = null;

	/**
	 * Initialize the ClientLook data members. It basically references
	 * the ClientBoardComponent's offscreen bitmap (which the look
	 * draws into) and builds a graphics context for the offscreen
	 * bitmap and the onscreen bitmap.
	 * @param c the ClientBoardComponent we are drawing into
	 */
	public ClientLook(ClientBoardComponent c) {
		offScreenImage = c.getOffScreenImage();
		offGC = offScreenImage.getGraphics();
		onGC = c.getGraphics();
		component = c;
		c.setLook(this);
	}

	/**
	 * Abstract method redefined in subclasses to actually physically
	 * update the graphical state of the board during the game.
	 */
	public abstract void update(ClientBoard b);

	/**
	 * Abstract method redefined in subclasses to update the 
	 * background terrain of the game board.
	 */
	public abstract void updateTerrain(ClientBoard b);

	/**
	 * Forces the offscreen graphic to draw on the ClientBoardComponent.
	 */
	public void updateDisplay() {
		onGC.drawImage(offScreenImage, 0, 0, component);
	}
}
