/*
 * (#)@QueuePanel.java
 */
package games.Battle.client.EuropaClient;

import java.awt.*;
import java.util.*;

import games.Battle.shared.comm.*;

/**
 * QueuePanel implements the panel of controls that the user sees for 
 * choosing a game queue. The user picks a game queue, and when the
 * queue fills up the game starts.
 */

class QueuePanel extends Panel {
	/**
	 * A table of all known game queue components
	 */
	Dictionary queueTable;
	/**
	 * A reference back to the actual game client
	 */
	EuropaClient client;

	/**
	 * construct the grid of the Queue panel.
	 * @param numGames how many games will appear, 
	 *                 expected to be a multiple of 4
	 */
	public QueuePanel(int numGames, EuropaClient client) {
		setBackground(Color.orange);
		int rows = numGames / 4;
		setLayout(new GridLayout(rows, 4, 2, 2));
		// setLayout(new CardLayout());
		queueTable = new Hashtable();
		this.client = client;
	}

	public void setGameInfo(GameInfo game) {
		QueueComponent q = (QueueComponent)queueTable.get(game);

		if (q == null) {
			q = new EuropaQueueComponent(game, client);
			queueTable.put(game, q);
			add(q);
		}
		else {
			q.resetGameInfo(game);
		}
	}

	public Dimension minimimSize() {
		return new Dimension(100*2, 60*4);
	}
	
	public Dimension preferredSize() {
		return new Dimension(140*2, 100*4);
	}
}
