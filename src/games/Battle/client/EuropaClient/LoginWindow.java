/*
 * @(#)LoginWindow.java
 */

package games.Battle.client.EuropaClient;

import java.awt.*;

import games.Battle.shared.comm.PlayerInfo;

/**
 * This dialog window is responsible for collecting login/creation
 * information about a player.
 */
public class LoginWindow extends Frame {

	/**
	 * The client applet.
	 */
	EuropaClient client;

	/**
	 * The container for all the dialog elements.
	 */
	Container info;

	/**
	 * The text field for the login id.
	 */
	TextField login;

	/**
	 * The text field for the password.
	 */
	TextField passwd;

	/**
	 * The text field for the name of the player.
	 */
	TextField name;

	/**
	 * The text field for the e-mail address of the player.
	 */
	TextField email;

	/**
	 * The panel for the buttons at the bottom of the dialog.
	 */
	Panel buttons;

	/**
	 * The ok button.
	 */
	Button ok;

	/** 
	 * Indicates which button has user focus.
	 */
	int focus = 0;

	/**
	 * A referncing array for all the buttons.
	 */
	Component[] fields;

	/**
	 * Construct a LoginWindow as a child of the given client applet.
	 * This constructor is used when first logging into the game.
	 * @param client the EuropaClient parent window
	 */
	public LoginWindow(EuropaClient client) {
		//super(null, "Authorization Required", true);
		super("Authorization Required");
		this.client = client;

		setupLogin();
	}

	/**
	 * Construct a LoginWindow with the given PlayerInfo. This 
	 * version of the constructor is used to allow players
	 * to modify their login information.
	 * @param client the EuropaClient parent applet
	 * @param info the player info to initialize the dialog with
	 */
	public LoginWindow(EuropaClient client, PlayerInfo info) {
		//super(null, "Change Information", true);
		super("Change Information");
		this.client = client;

		setupChangeInfo(info);
	}

	/**
	 * Set up the fields for a login dialog.
	 */
	void setupLogin() {
		// kludge: couldn't find a way to reset the size of the spacing for
		// the borderlayout that we have by default.
		setLayout(new BorderLayout(80, 10));

		if (info != null)
			info.removeNotify();

		info = new Panel();
		info.setLayout(new GridLayout(2, 2, 4, 4));
		info.add(new Label("                login:", Label.RIGHT));
		info.add(login = new TextField());
		info.add(new Label("             password:", Label.RIGHT));
		info.add(passwd = new TextField());
		name = null;
		email = null;
		passwd.setEchoCharacter('*');
		add("West", info);
		add("North", new Panel());

		if (buttons != null)
			buttons.removeNotify();

		buttons = new Panel();
		Button cancel;
		Button help;
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));
		buttons.add(ok = new Button("Login"));
		buttons.add(cancel = new Button("Cancel"));
		buttons.add(help = new Button("I'm new"));
		add("South", buttons);

		if (fields != null) {
			for (int i = 0; i < fields.length; i++)	
				fields[i].removeNotify();
		}

		fields = new Component[5];
		fields[0] = login;
		fields[1] = passwd;
		fields[2] = ok;
		fields[3] = cancel;
		fields[4] = help;
		focus = 0;

		pack();

		login.requestFocus();
	}

	/**
	 * Setup the dialog with new player components.
	 */
	void setupPlayerInfo() {
		// kludge: couldn't find a way to reset the size of the spacing for
		// the borderlayout that we have by default.
		setLayout(new BorderLayout(80, 10));

		if (info != null)
			info.removeNotify();

		info = new Panel();
		info.setLayout(new GridLayout(4, 2, 4, 4));
		info.add(new Label("                login:", Label.RIGHT));
		info.add(login = new TextField());
		info.add(new Label("         new password:", Label.RIGHT));
		info.add(passwd = new TextField());

		info.add(new Label("            real name:", Label.RIGHT));
		info.add(name = new TextField());
		info.add(new Label("    (optional) e-mail:", Label.RIGHT));
		info.add(email = new TextField());

		passwd.setEchoCharacter('*');
		add("West", info);
		add("North", new Panel());

		if (buttons != null)
			buttons.removeNotify();

		buttons = new Panel();
		Button cancel;
		Button help;
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));
		buttons.add(ok = new Button("Login"));
		buttons.add(cancel = new Button("Cancel"));
		buttons.add(help = new Button("Back"));
		add("South", buttons);

		if (fields != null) {
			for (int i = 0; i < fields.length; i++)	
				fields[i].removeNotify();
		}

		fields = new Component[7];
		fields[0] = login;
		fields[1] = passwd;
		fields[2] = name;
		fields[3] = email;
		fields[4] = ok;
		fields[5] = cancel;
		fields[6] = help;
		focus = 0;

		pack();

		login.requestFocus();
	}

	/**
	 * Set up the dialog with player change info components.
	 */
	void setupChangeInfo(PlayerInfo id) {
		// kludge: couldn't find a way to reset the size of the spacing for
		// the borderlayout that we have by default.
		setLayout(new BorderLayout(80, 10));

		if (info != null)
			info.removeNotify();

		info = new Panel();
		info.setLayout(new GridLayout(4, 2, 4, 4));
		info.add(new Label("                login:", Label.RIGHT));
		info.add(login = new TextField());
		login.setEditable(false);
		login.setText(id.getHandle());

		info.add(new Label("         new password:", Label.RIGHT));
		info.add(passwd = new TextField());
		passwd.setEchoCharacter('*');
		passwd.setText(id.getPassword());

		info.add(new Label("            real name:", Label.RIGHT));
		info.add(name = new TextField());
		name.setText(id.getName());
		info.add(new Label("    (optional) e-mail:", Label.RIGHT));
		info.add(email = new TextField());
		email.setText(id.getMail());

		add("West", info);
		add("North", new Panel());

		if (buttons != null)
			buttons.removeNotify();

		buttons = new Panel();
		Button cancel;
		Button help;
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 10));
		buttons.add(ok = new Button("Update"));
		buttons.add(cancel = new Button("Don't Update"));
		add("South", buttons);

		if (fields != null) {
			for (int i = 0; i < fields.length; i++)	
				fields[i].removeNotify();
		}

		fields = new Component[7];
		fields[0] = login;
		fields[1] = passwd;
		fields[2] = name;
		fields[3] = email;
		fields[4] = ok;
		fields[5] = cancel;
		focus = 0;

		hide();
		pack();
		show();

		login.requestFocus();
	}

	/**
	 * Process events in the dialog box.
	 */
    public boolean handleEvent(Event e) {
		boolean doneLogin = false;

        if (e.id == Event.WINDOW_DESTROY || e.arg == "Cancel") {
			client.setPlayerInfo(null);
			client.dialogDone();
			dispose();
        }
		else if (e.arg == "Don't Update") {
			client.dialogDone();
			dispose();
		}
		else if (e.id == Event.KEY_RELEASE) {
			if ((char)e.key == '\t' || (char)e.key == '\n') {
				focus++;
				focus %= fields.length;
				if (fields[focus] == ok && (char)e.key == '\n')
					doneLogin = true;
				fields[focus].requestFocus();
			}
		}
		else if (e.arg == "Login" || e.arg == "Update") {
			doneLogin = true;
		}
		else if (e.arg == "I'm new") {
			setupPlayerInfo();
		}
		else if (e.arg == "Back") {
			setupLogin();
		}

		if (doneLogin) {
			String realname;
			String emailaddr;

			if (name != null)
				realname = name.getText();
			else
				realname = "";

			if (email != null)
				emailaddr = email.getText();
			else
				emailaddr = "";

			client.setPlayerInfo(new PlayerInfo(login.getText(), 
														realname, emailaddr,
														passwd.getText()));
			client.dialogDone();
			dispose();
		}
		return super.handleEvent(e);
    }

	/**
	 * Overload the layout method to pack() as well.
	 */
	public synchronized void layout() {
		super.layout();
	}
}
