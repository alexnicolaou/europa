/*
 * @(#)EuropaClient.java
 */
package games.Battle.client.EuropaClient;

import java.applet.*;
import java.awt.*;
import java.net.*;
import java.io.*;

import win.ClosableFrame.*;
import games.Battle.client.ClientApplet.*;
import games.Battle.shared.sys.*;
import games.Battle.shared.comm.*;
import games.Battle.client.EuropaClient.*;

interface FinderTest {
	public boolean checkCell(int row, int col);
}

class CellFinder {
	int lastRow = 0;
	int lastCol = 0;
	FinderTest test;

	public CellFinder(FinderTest test) {
		this.test = test;
	}

	public boolean findNextCell() {
		int startRow = lastRow;
		int startCol = lastCol;

		wrapped = false;
		while(true) {
			lastCol++;
			if (lastCol >= Rules.cols) {
				lastCol = 0;
				lastRow++;
				if (lastRow >= Rules.rows) {
					lastRow = 0;
					wrapped = true;
				}
			}
			if (lastCol == startCol && lastRow == startRow)
				return false;

			if (test.checkCell(lastRow, lastCol))
				return true;
		}
	}

	boolean wrapped = false;
	public boolean justWrapped() { return wrapped; }

	public int getRow() { return lastRow; }
	public int getCol() { return lastCol; }
}

class MySquareTest implements FinderTest{
	int me;
	Board board;
	public MySquareTest() {
	}
	public void setPlayer(int player) { me = player; }
	public void setBoard(Board b) { board = b; }

	public boolean checkCell(int row, int col) {
		Cell cell = board.getCell(row, col);
		return cell.getOccupancy() == me ;//&& cell.getCity() > 0;
	}
}
class MyCityTest implements FinderTest{
	int me;
	Board board;
	public MyCityTest() {
	}
	public void setPlayer(int player) { me = player; }
	public void setBoard(Board b) { board = b; }

	public boolean checkCell(int row, int col) {
		Cell cell = board.getCell(row, col);
		return cell.getOccupancy() == me && cell.getCity() > 0;
	}
}
class EnemyCityTest implements FinderTest{
	int me;
	Board board;
	public EnemyCityTest() {
	}
	public void setPlayer(int player) { me = player; }
	public void setBoard(Board b) { board = b; }

	public boolean checkCell(int row, int col) {
		Cell cell = board.getCell(row, col);
		return cell.getOccupancy() != me && cell.getCity() > 0;
	}
}
class EnemyPipeTest implements FinderTest{
	int me;
	Board board;
	public EnemyPipeTest() {
	}
	public void setPlayer(int player) { me = player; }
	public void setBoard(Board b) { board = b; }

	public boolean checkCell(int row, int col) {
		Cell cell = board.getCell(row, col);
		if (cell.getOccupancy() != me) {
			boolean[] pipes = cell.getPipes();
			for (int i = 0; i < pipes.length; i++) 
				if (pipes[i]) return true;
		}
		return false;
	}
}
class AIBoardListener implements BoardListener {
    OutputStream os;
    int player = -1;

    byte turnNumber;

    public void setTurn(byte tn) {
        turnNumber = tn;
    }
    public void setPlayer(int p) {
        player = p;
		squareTest.setPlayer(p);
		cityTest.setPlayer(p);
		enemyTest.setPlayer(p);
		pipeTest.setPlayer(p);
    }
    public void setOutputStream(OutputStream o) {
        os = o;
    }

    public AIBoardListener() {
		sqRating = new int[Rules.rows][Rules.cols];
		visible = new boolean[Rules.rows][Rules.cols];
	}

	MySquareTest squareTest = new MySquareTest();
	CellFinder squareFinder = new CellFinder(squareTest);

	MyCityTest cityTest = new MyCityTest();
	CellFinder cityFinder = new CellFinder(cityTest);

	EnemyCityTest enemyTest = new EnemyCityTest();
	CellFinder enemyFinder = new CellFinder(enemyTest);

	EnemyPipeTest pipeTest = new EnemyPipeTest();
	CellFinder pipeFinder = new CellFinder(pipeTest);

	public boolean setPipe(int r, int c, int dir) {
		Cell cell = board.getCell(r, c);
		boolean[] pipes = cell.getPipes();
		if (pipes[dir] == false) {
			Command pipe = new Command(Symbols.PLAYER0, (short)dir,
									   r, c, 1, 1, turnNumber);
			try {
				pipe.writeTo(os);
				return true;
			}
			catch (IOException e) {
			}
		}
		return false;
	}
	final static int intabs(int x) { if (x > 0) return x; else return -x; }
	public void setStandardPipes(int row, int col) {
		boolean didsomething = false;
		for (int i = 0; i < 15 && didsomething == false; i++) {
			int cityrow = -1;
			int citycol = -1;
			int lowestmanhattan = 9999;
			while (cityFinder.justWrapped() == false) {
				if (cityFinder.findNextCell() == false) 
					return;
				int manhattan = intabs(row - cityFinder.getRow());
				manhattan += intabs(col - cityFinder.getCol());
				if (manhattan < lowestmanhattan) {
					cityrow = cityFinder.getRow();
					citycol = cityFinder.getCol();
					lowestmanhattan = manhattan;
				}
			}
			if (cityrow != -1) {
				if (cityrow > row) {
					// set a south pipe
					didsomething |= setPipe(row, col, Symbols.SOUTH);
				}
				else if (cityrow < row) {
					// set a north pipe
					didsomething |= setPipe(row, col, Symbols.NORTH);
				}
				else { 
					// set both north and south pipes
					didsomething |= setPipe(row, col, Symbols.SOUTH);
					didsomething |= setPipe(row, col, Symbols.NORTH);
				}
			}
			if (citycol != -1) {
				if (citycol > col) {
					// set an east pipe
					didsomething |= setPipe(row, col, Symbols.EAST);
				}
				else if (citycol < col) {
					// set a west pipe
					didsomething |= setPipe(row, col, Symbols.WEST);
				}
				else { 
					// set both ew pipes
					didsomething |= setPipe(row, col, Symbols.EAST);
					didsomething |= setPipe(row, col, Symbols.WEST);
				}
			}
		}
	}

	ClientBoard board;
    public void boardChanged(ClientBoard board) {
		this.board = board;
        dump(board);

		squareTest.setPlayer(player);
		cityTest.setPlayer(player);
		enemyTest.setPlayer(player);
		pipeTest.setPlayer(player);
		squareTest.setBoard(board);
		cityTest.setBoard(board);
		if (squareFinder.findNextCell()) {
			Command cmd = null;
			int row = squareFinder.getRow();
			int col = squareFinder.getCol();
			setStandardPipes(row, col);
			int times = 1;

			if (true || (turnNumber >= 24 && (turnNumber % 10 == 0))) {

				if (turnNumber > 49) {
					int resAmt = 4;
					cmd = new Command(Symbols.PLAYER0, Symbols.RESERVES, 
									row, col, resAmt, 0, turnNumber);
				}

			}
			else if (turnNumber < 24) {
				int offx = 0;
				int offy = 0;
				switch(turnNumber % 4) {
				case 0:
					offx = -2;
					offy = -2;
					break;
				case 1:
					offx = +2;
					offy = -2;
					break;
				case 2:
					offx = +2;
					offy = +2;
					break;
				default:
				case 3:
					offx = -2;
					offy = +2;
					break;
				}
				if (row + offy < 0) offy = -row;
				if (row + offy >= Rules.rows) offy = (Rules.rows - row - 1);
				if (col + offx < 0) offx = -col;
				if (col + offx >= Rules.cols) offx = (Rules.cols - col - 1);

				System.out.println("paratroop from " + row + ", " + col +
						" to " + (row + offy) + ", " + (col+offx));
				cmd = new Command(Symbols.PLAYER0, Symbols.PARATROOP, row, col, 
											row+offy, col+offx, turnNumber);
				times = 3;
				
			}
			if (cmd != null) {
				try {
					for (int i = 0; i < times; i++)
						cmd.writeTo(os);
				} catch ( Exception e ) {
					// where's the server?
					println("Exception: " + e);
				}
			}
		}
		pipeTest.setBoard(board);
		enemyTest.setBoard(board);
		if (enemyFinder.findNextCell()) {
			int troops = 0;
			Command gun = null;
			Command para = null;
			int row = enemyFinder.getRow();
			int col = enemyFinder.getCol();
			int times = 1;

			for (int offx = -2; offx < 3; offx++) {
				for (int offy = -2; offy < 3; offy++) {
					if (offx == 0 && offy == 0) continue;
					if (row + offy < 0) continue;
					if (row + offy >= Rules.rows) continue;
					if (col + offx < 0) continue;
					if (col + offx >= Rules.cols) continue;

					try {
						Cell dest = board.getCell(row + offy, col + offx);
					if (dest.getTroops() > troops && dest.getOccupancy() == player) {
						gun = new Command(Symbols.PLAYER0, Symbols.GUN, 
											row + offy, col + offx, 
											row, col, turnNumber);
						para = new Command(Symbols.PLAYER0, Symbols.PARATROOP, 
											row + offy, col + offx, 
											row, col, turnNumber);
						troops = dest.getTroops();
					}
					}
					catch(Exception e) {
						System.out.println("caught " + e);
						e.printStackTrace();
					}
				}
			}
			if (gun != null) {
				try {
					int i;
					gun.writeTo(os);
					gun.writeTo(os);
					gun.writeTo(os);
					for (i = 0; i < 2; i++)
						para.writeTo(os);
				} catch ( Exception e ) {
					// where's the server?
					println("Exception: " + e);
				}
			}
		}
		if (pipeFinder.findNextCell()) {
			int troops = 0;
			Command gun = null;
			Command para = null;
			int row = pipeFinder.getRow();
			int col = pipeFinder.getCol();
			int times = 1;

			for (int offx = -2; offx < 3; offx++) {
				for (int offy = -2; offy < 3; offy++) {
					if (offx == 0 && offy == 0) continue;
					if (row + offy < 0) continue;
					if (row + offy >= Rules.rows) continue;
					if (col + offx < 0) continue;
					if (col + offx >= Rules.cols) continue;

					try {
						Cell dest = board.getCell(row + offy, col + offx);
					if (dest.getTroops() > troops && dest.getOccupancy() == player) {
						gun = new Command(Symbols.PLAYER0, Symbols.GUN, 
											row + offy, col + offx, 
											row, col, turnNumber);
						para = new Command(Symbols.PLAYER0, Symbols.PARATROOP, 
											row + offy, col + offx, 
											row, col, turnNumber);
						troops = dest.getTroops();
					}
					}
					catch(Exception e) {
						System.out.println("caught " + e);
						e.printStackTrace();
					}
				}
			}
			if (gun != null) {
				try {
					int i;
					for (i = 0; i < 3; i++)
						gun.writeTo(os);
					for (i = 0; i < 3; i++)
						para.writeTo(os);
				} catch ( Exception e ) {
					// where's the server?
					println("Exception: " + e);
				}
			}
		}
    }

	int invRating = 300;
    static int lastRow = 0;
    static int lastCol = 0;
	int sqRating[][];
	boolean visible[][];

	int getMoveRating(int row, int col, Cell cell) {
		if (row < 0 || col < 0 || row >= Rules.rows || col >= Rules.cols) {
			return 0;
		}
		Cell dest = board.getCell(row, col);
		if (dest.getOccupancy() == Symbols.INVISIBLE && visible[row][col] == false)
			return invRating;
		visible[row][col] = true;
		int occAdjust = 0;
		if (dest.getOccupancy() == player /*&& (dest.getCity() > 0 || dest.getTroops() > 0)*/) {
			boolean retZero = false;
			boolean[] pipes = cell.getPipes();
			for (int i = 0; i < pipes.length; i++) {
				retZero = retZero || pipes[i];
			}
			if (retZero) 
				return 0;
			occAdjust = -700;
		}
		else if (dest.getTroops() > 1) {
			if (dest.getTroops() < 3)
				occAdjust = +500;
			else if (dest.getTroops() < cell.getTroops())
				occAdjust = +100;
			else 
				occAdjust = -400;
		}

		return baseRating(row, col, cell) + 
				//(cell.getTroops()-30)*4 + 
				occAdjust;
	}
	int getRating(int row, int col, Cell cell) {
		return getMoveRating(row, col, cell);
	}

	int cityValue = 2000;
	int baseRating(int row, int col, Cell cell) {
		if (row < 0 || col < 0 || row >= Rules.rows || col >= Rules.cols) {
			return 0;
		}
		Cell dest = board.getCell(row, col);
		if (dest.getOccupancy() == Symbols.INVISIBLE && 
			visible[row][col] == false)
			return invRating;
		visible[row][col] = true;

		if (dest.getCity() > 0) {
			if (cell.getOccupancy() == player) {
				return 600;
			}
			else {
				return cityValue;// + (32 - dest.getTroops())*10;
			}
		}
		else if (dest.getTerrain() == 0)
			return 0;

		int opponent = 0;
		if (dest.getOccupancy() != player) 
			opponent = 100;

		int terrainDiff = dest.getTerrain() - cell.getTerrain();
		terrainDiff = terrainDiff + 7;

		return 100 + opponent - terrainDiff*5;
	}

	int recomputeRating(int row, int col, Cell cell) {
		int ret = 150;
		if (cell.getCity() > 0) {
			if (cell.getOccupancy() == player) {
				ret = 600;
			}
			else {
				ret = 800;
			}
		}
		else if (cell.getTerrain() == 0)
			ret = 0;
		else {
			int maxRating = 0;
			int r = getRating(row-1, col, cell);
			if (r > maxRating) maxRating = r;

			r = getRating(row+1, col, cell);
			if (r > maxRating) maxRating = r;

			r = getRating(row, col-1, cell);
			if (r > maxRating) maxRating = r;

			r = getRating(row, col+1, cell);
			if (r > maxRating) maxRating = r;

			if (maxRating < 500)
				ret = maxRating - 5;
			else
				ret = maxRating - 50;
		}


		if (ret < 100 && cell.getTerrain() != 0) {
			ret = 100;
		}
		return ret;
	}

	public void print(String s) {
		System.out.print(s);
	}
	public void println(String s) {
		System.out.println(s);
	}
	public void println() {
		System.out.println();
	}

    public void dump(ClientBoard board) {
        int moves = 0;
        boolean reset = true;
        int iter = 0;
        while (reset && iter < 2) {
            iter++;
			Command cmd = null;
			int bestRated = 10;
            for (int r=0; r< Rules.rows; r++) {
				for (int layer = 0; layer < 3; layer++) {
					if (layer == 0) {
						for (int c=0; c< Rules.cols; c++) {
							ClientCell cell = (ClientCell)board.getCell(r,c);
							sqRating[r][c] = recomputeRating(r, c, cell);
							if (cell.getOccupancy() == player) reset = false;
							if (cell.getOccupancy() == player && moves < 8 &&
							((r == lastRow && c >= lastCol) || (r > lastRow))) {
								//lastRow = r;
								//lastCol = c;
								int oldMoves = moves;
								boolean[] pipes = cell.getPipes();
								if (cell.getTroops() > 1)
									for (short i = 0; i < pipes.length; i++) {
										if (pipes[i] == false) {
											//moves++;
											Command tempC = new 
												Command(Symbols.PLAYER0, i,
													  r, c, 1, 1, turnNumber);
											int coff = 0; int roff = 0;
											switch (i) {
											case Symbols.NORTH:
												roff = -1;
												break;
											case Symbols.SOUTH:
												roff = +1;
												break;
											case Symbols.EAST:
												coff = +1;
												break;
											case Symbols.WEST:
												coff = -1;
												break;
											}
											int newRow = r + roff;
											int newCol = c + coff;
											int tempR = getMoveRating(newRow, newCol, cell);
											if (tempR > bestRated) {
												bestRated = tempR;
												cmd = tempC;
											}
										}
									}
								if (moves > oldMoves)
									print("(M) ");
								else 
									print("(m) ");
								continue;
							}
							switch (cell.getOccupancy()) {
								case Symbols.PLAYER0:
									print("(0) ");
									break;
								case Symbols.PLAYER1:
									print("(1) ");
									break;
								case Symbols.PLAYER2:
									print("(2) ");
									break;
								case Symbols.PLAYER3:
									print("(3) ");
									break;
								case Symbols.PLAYER4:
									print("(4) ");
									break;
								case Symbols.UNOCCUPIED:
									print("(.) ");
									break;
								case Symbols.INVISIBLE:
									print("(X) ");
									break;
								case Symbols.UNMODIFIED:
									print("(U) ");
									break;
								default:
									print("(?) ");
									break;
							}
						}
					}
					else if (layer == 1) {
						for (int c=0; c< Rules.cols; c++) {
							if (sqRating[r][c] == 0) {
								print("000 ");
							}
							else {
								print("" + sqRating[r][c] + " ");
							}
						}
					}
					else {
						for (int c=0; c< Rules.cols; c++) {
							if (visible[r][c]) {
								print("(V) ");
							}
							else {
								print("--- ");
							}
						}
					}
					println();
				}
            }

			if (cmd != null) {
				try {
					cmd.writeTo(os);
				} catch ( Exception e ) {
					// where's the server?
					println("Exception: " + e);
				}
			}
			break;
        }
        println("row,col = " + lastRow + ", " + lastCol);
    }
}

/**
 * @author Alex Nicolaou
 */
public class Blob extends EuropaClient {
    public Blob(Frame f) {
        super(f);
    }

    /**
     * The main routine for an applet-less execution.
     */
    public static void main(String argv[]) {
        Frame f = new ClosableFrame("Europa");
        Blob client = new Blob(f);
        f.add("Center", client);
        client.init();
        client.start();
    }

    public void processChatCommand(String command) {
		int ji;
		if (command.equals("help")) {
			sendChat("To get me to join a game, type:");        
			sendChat("@blob join <game #>");
			sendChat("for example: @blob join 0");
			sendChat("if I'm already in a game, getting me to join");
			sendChat("again will take me out.");
			return;
		}
		else if ((ji = command.indexOf("join", 0)) != -1) {
			int si = command.indexOf(" ", ji);
			String remainder = command.substring(si+1);
			int game = Integer.valueOf(remainder).intValue();
			sendChat("ok, joining game #" + game);
			try {
			join(game);
			}catch(Exception e) {}
			return;
		}
        sendChat("I got the command [" + command + "]");        
        sendChat("I'm sorry but I don't work yet, so I don't");
        sendChat("understand. Try talking to Osric.");
    }

    String key = "@blob ";
    public void checkChatMsg(String msg) {
        int findblob = msg.indexOf(key, 0);
        if (findblob != -1 && msg.indexOf("BlobRobot:", 0) != 0)
            processChatCommand(msg.substring(findblob + key.length()));
    }

    public void oldstart() {
        // send a registration
        InfoPacket loginResult = new InfoPacket();
        int tries = 0;

        id = new PlayerInfo("BlobRobot",  "by Alex Nicolaou", "type @blob help to use me", "theblobbot");

        try {
            id.writeTo(os);
        } 
        catch (Exception e) {
            Window err;
            err = new ErrorWindow(this,"Connection lost.");
            System.out.println(e.toString());
            e.printStackTrace();
            err.show();
            dialogWait();
            return;
        }

        try {
            loginResult.readFrom(is);
        }
        catch (Exception e) {
            Window err;
            err = new ErrorWindow(this,"Connection lost.");
            System.out.println(e.toString());
            e.printStackTrace();
            err.show();
            dialogWait();
            return;
        }

        if (loginResult.isFatal()) {
            Window err;
            err = new ErrorWindow(this, loginResult.toString());
            err.show();
            dialogWait();
        }
        tries++;

        if (loginResult.isFatal()) {
            return;
        }

        // read in all the info on this user
        try {
            id.readFrom(is);
        }
        catch (Exception e) {
            Window err;
            err = new ErrorWindow(this,"Connection lost.");
            System.out.println(e.toString());
            e.printStackTrace();
            err.show();
            dialogWait();
            return;
        }

        if (id.isWizard())
            buttonBar.add(shutdownButton);

        setLayout(new BorderLayout());

        //clientFrame.setBackground(Color.orange);
        //clientFrame.resize(400, 500);
        clientFrame.validate();
        clientFrame.pack();
        clientFrame.show();
        clientFrame.validate();
        clientFrame.pack();
        clientFrame.repaint();

        //run();
    }

    public void startGame() {
        // time to actually start the game
        ClientApplet client;
        AIBoardListener ai = new AIBoardListener();
        client = new ClientApplet(ai, id, this, host, 5100+gameId);
        Frame f = new ClientFrame("Europa Control", client);

        client.init();
        f.add("Center", client);
        f.pack();
        f.show();

        client.start();
        // f.pack();

        // make sure we never start two clients for the same game
        gameId = -1;
    }
}
