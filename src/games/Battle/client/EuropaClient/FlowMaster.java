/*
 * @(#)EuropaClient.java
 */
package games.Battle.client.EuropaClient;

import java.applet.*;
import java.awt.*;
import java.net.*;
import java.io.*;

import win.ClosableFrame.*;
import games.Battle.client.ClientApplet.*;
import games.Battle.shared.sys.*;
import games.Battle.shared.comm.*;

/**
 * EuropaClient is the main applet for the "lobby" of the Europa
 * game. Sets up the game queue graphics, the who-list and the
 * chat window. This applet also initiates conversations with
 * the game server, logs users in, and spawns game board
 * clients when the server instructs it that a game is about to
 * begin.
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class FlowMaster extends EuropaClient {
    public FlowMaster(Frame f) {
	super(f);
    }
}
