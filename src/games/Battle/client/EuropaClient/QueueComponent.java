/*
 * @(#)QueueComponent.java
 */

package games.Battle.client.EuropaClient;

import java.awt.*;

import games.Battle.shared.comm.*;

/**
 * QueueComponent is a base class allowing various implementations
 * of graphics to display the waiting queue's for games on the
 * Europa server.
 */
public abstract class QueueComponent extends Canvas {

	/** 
	 * The GameInfo instance for this queue.
	 */
	GameInfo game;

	/**
	 * The client applet.
	 */
	EuropaClient client;

	/**
	 * Construct a QueueComponent.
	 */
	QueueComponent(GameInfo game, EuropaClient client) {
		this.game = game;
		this.client = client;
	}

	/**
	 * The method to hilite a queue component, when the mouse moves
	 * into the window. This method is implemented in subclasses
	 * to provided behaviour.
	 */
	public abstract void hilite();

	/**
	 * The method to normalize or unhilite a queue component, when the
	 * mouse moves out of the window. This method is implemented in
	 * subclasses to provide behaviour.
	 */
	public abstract void normal();

	/**
	 * Notifies the QueueComponent that the information associated with
	 * a particular game has changed and needs to be updated. A repaint
	 * is automatically called so that the component will update itself.
	 */
	public void resetGameInfo(GameInfo g) {
		game = g;
		repaint();
	}

	/**
	 * Process the event that occurs when the mouse enters the 
	 * component. Calls hilite().
	 * @param e the event
	 * @param x the x location
	 * @param y the y location
	 * @see #hilite
	 */
	public boolean mouseEnter(Event e, int x, int y) {
		super.mouseEnter(e, x, y);
		hilite();
		return true;
	}

	/**
	 * Process the event that occurs when the mouse exits the 
	 * component. Calls normal().
	 * @param e the event
	 * @param x the x location
	 * @param y the y location
	 * @see #normal
	 */
	public boolean mouseExit(Event e, int x, int y) {
		super.mouseExit(e, x, y);
		normal();
		return true;
	}

	/**
	 * Process a mouse up event. Tries to join a particular game
	 * queue.
	 * @param e the event
	 * @param x the x location
	 * @param y the y location
	 */
	public boolean mouseUp(Event e, int x, int y) {
		super.mouseUp(e, x, y);
		normal();
		try {
			client.join(game.getId());
		}
		catch (Exception err) {
			System.out.println("Exception: "+err);
		}
		hilite();
		return true;
	}

}
