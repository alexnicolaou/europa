import java.util.*;
import games.Rating.*;

public class rate {

	static public void main(String[] argv) {
		Rating r1 = new Rating(1650, 80);
		Rating r2 = new Rating(1650, 80);
		Rating r3 = new Rating();
		Rating[] game = { r1, r2 };

		Random rand = new Random();
		int p1Wins = 2;

		for (int gnum = 0; gnum < 10; gnum++) {

			//int roll = Math.abs(rand.nextInt()) % 10;
			//if (roll < p1Wins) {
				//game[0] = r2;
				//game[1] = r1;
			//}
			//else {
				//game[0] = r1;
				//game[1] = r2;
			//}
			//if (gnum % 5 != 0) {
				game[0] = r1;
				game[1] = r2;
			//}
			//else {
				//game[0] = r2;
				//game[1] = r1;
			//}

			Rating.calculateRatings(game);
			System.out.print("Player 1: " + r1 + ", ");
			System.out.print("Player 2: " + r2 + ", ");
			//System.out.print("Player 3: " + r3 + "  ");
			double pow10 = Math.pow(10.0, (r1.getRating() - r2.getRating())/400.0);
			double probP1 = pow10 / (1.0 + pow10);
			System.out.println("Probability = " + probP1);
		}
	}
}
